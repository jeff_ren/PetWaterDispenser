#ifndef __TEST_H
#define __TEST_H

#include "stm32f0xx.h"
void Test_Calib(void);
void EXTI4_15_IRQHandler(void);
void TEST_Init(void);
void test(void);
void TEST_Toggle(void);
#endif
