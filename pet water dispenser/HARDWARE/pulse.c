#include "pulse.h"
#include "check.h"
#include "task.h"
#include "delay.h"

uint8_t open_flag = 0;
uint8_t sen_data = 0,frame_data = 0;
uint16_t sen_num = 0,sen_len = 0;
uint8_t Rx_Flag = 0;
uint8_t sen_table[100];
/***********************************
  * @brief  定时器3输入捕获模式引脚配置
  * @param  none
  * @retval none
************************************/
void Pulse_In_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	//管脚配置 PA9
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;            //复用模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;  //高速输出
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;       //推挽输出
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_NOPULL;       //无上下拉
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_2); //复用配置
	
}

/***********************************
  * @brief  输出模式引脚配置
  * @param  none
  * @retval none
************************************/
void Pulse_Out_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	//管脚配置 PA9

	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;           //输出模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;  //高速
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;       //推挽输出
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_NOPULL;       //无上下拉
	GPIO_Init(GPIOA,&GPIO_InitStructure);

	Pluse_1;
}

extern uint8_t pulse_flag ;
extern uint8_t PULSE_TIME ;
/***********************************
  * @brief  输出起始码
  * @param  none
  * @retval none
************************************/
void Pulse_Start(void) 
{
  Pluse_0;
	delay_us(100);  
	
  Pluse_1;
	delay_us(300);  
	
  Pluse_0;
	delay_us(100);  
}

/***********************************
  * @brief  输出结束码
  * @param  none
  * @retval none
************************************/
void Pulse_Stop(void) 
{
  Pluse_1;
	delay_us(400);  
	
  Pluse_0;
	delay_us(100); 
	
		Pluse_1;
}

/***********************************
  * @brief  输出0
  * @param  none
  * @retval none
************************************/
void Send_Data0(void) 
{
  Pluse_1;
	delay_us(100);  
	
  Pluse_0;
	delay_us(100);  
}

/***********************************
  * @brief  输出1
  * @param  none
  * @retval none
************************************/
void Send_Data1(void) 
{
  Pluse_1;
	delay_us(200);  
	
  Pluse_0;
	delay_us(100);  
}

/***********************************
  * @brief  输出1个字节
  * @param  none
  * @retval none
************************************/
void Pulse_SendByte(uint8_t Byte) 
{
  uint8_t i;
  for(i=0;i<8;i++)
  {
    if(Byte & 0x80)
    {
      Send_Data1();
    }
    else 
    {
      Send_Data0();
    }
    Byte <<= 1;      
  }
}

/***********************************
  * @brief  发送一个数据包
  * @param  数据，数据长度
  * @retval none
************************************/
void Pulse_SendData(uint8_t pluse_data[],uint8_t len)
{
  uint8_t j;
	TIM_ITConfig(TIM1, TIM_IT_CC2, DISABLE); // 屏蔽定时器中断
	TIM_Cmd(TIM1, DISABLE); //关闭定时器
	TIM1->CCER &= (uint16_t)~((uint16_t)TIM_CCER_CC2E);  // 关闭捕获模式第2通道
  Pulse_Out_Config();  // 设置引脚为输出模式
  delay_ms(1);
  Pulse_Start();
  for(j=0;j<len;j++)
  {		
    Pulse_SendByte(pluse_data[j]); 
  }
  Pulse_Stop();
  delay_ms(1);
	Pulse_In_Config();  //设置引脚为复用定时器中断捕获模式
	TIM1->CCER |= (uint16_t)TIM_CCER_CC2E; // 使能 捕获模式第2通道
	TIM_ITConfig(TIM1, TIM_IT_CC2, ENABLE); //使能 定时器中断
	TIM_Cmd(TIM1, ENABLE); // 使能定时器
}


extern uint16_t capture ;
extern uint8_t Pulse_Rx_Flag;
extern uint16_t capture_value1, capture_value2;
extern uint8_t FristInFlag;
uint8_t trans_flag = 0; 
uint8_t data_flag = 0 ;
extern uint8_t Frist_Cap ;
uint8_t my_flag=0 ;
/***********************************
  * @brief  脉冲接收函数
  * @param  none
  * @retval none
************************************/
void Pulse_RxDispose(void)
{
		while ( trans_flag == 0 ) // 捕获数据直到获得STOP命令 
	{
		while(!capture); // 等待捕获数据
		if( ((capture > (4 * 4800 - 1500 )) && (capture < (4 * 4800 + 1500 ))))  //start (48m hz) 300us high 100us low
		{
			open_flag = 1; // 开始存入sen_table数组
			data_flag = 1;
		}
		else if ( ((capture > ( 2 * 4800 - 1500 )) && (capture < ( 2 * 4800 + 1500 ))) )  //0  100us high 100us low
		{
			sen_data = 0;
			data_flag = 1;
		}
		else if (((capture > (3 * 4800 - 1500 )) && (capture < (3 * 4800 + 1500 ))))  //1  200us high 100us low
		{
			sen_data = 1;	
			data_flag = 1 ;			
		}
		else if (((capture > (5 * 4800 - 1800 )) && (capture < (5 * 4800 + 2500 ))))  //stop 400us high 100us low 
		{
			trans_flag = 1 ; // 跳出接收数据
			open_flag = 0; // 重置参数
			sen_num = 0;
			sen_len = 0;
			Rx_Flag = 1;
			capture_value1 = 0 ;
			capture_value2 = 0 ;
		}
		else
		{ 
			sen_num = 0;
			sen_len = 0;
			open_flag = 0; 
			capture = 0 ;
			break;
		}
		if( open_flag == 1 && data_flag == 1) // 存入数据
		{
			data_flag = 0 ;
			frame_data <<= 1;
      frame_data += sen_data;
      if(sen_num % 8 == 0)
      { 
        sen_len = sen_num/8;
        sen_table[sen_len-1] = frame_data;
        frame_data = 0;		
      }
     sen_num++;
		}
		if(sen_len > 40 )
			break;
	capture = 0 ;  // 用于等待下次接收
	}
}

/***********************************
  * @brief  数据接收函数
  * @param  none
  * @retval none
************************************/
void Data_RxDispose(void)
{
  uint16_t chksum = 0,chksummcu = 0,Cmd = 0;
	Pulse_RxDispose();//接收数据时间为20ms
	Frist_Cap = 0 ;
	my_flag = 0 ; 
  if(Rx_Flag == 1)//数据处理时间为52ms
  {
    chksum = sen_table[0] + (uint16_t)(sen_table[1]<<8);
    chksummcu = SUM_CHECK(sen_table+2,sen_table[4]+6);
    Cmd = sen_table[2] + (uint16_t)(sen_table[3]<<8);
    if(chksum == chksummcu)                        
    {
      switch(Cmd)
      {
        case(0x0b01):
        if(sen_table[8] == 0x08)                 
        {
          Parameter_Query();                     //传感器信息查询 
        }
        else if(sen_table[8] == 0x09)            
        {
          Data_Query();                     //传感器数据查询
        } 

        break;
        case(0x0b02):                        //设置指令
        if(sen_table[8] == 0x0c)             
        {
          Correct_Process();                 //校准
        }
        else if(sen_table[8] == 0x0f)
        {
          Set_Message_Process();            //信息设置
        }
   
        break;
        default:;break;
      }
    }
    Rx_Flag = 0;
  }
}

