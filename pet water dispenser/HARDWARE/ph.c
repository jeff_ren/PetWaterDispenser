#include "ph.h"
#include "adc.h"
#include "flash.h"
#include "delay.h"
#include "test.h"

extern uint8_t Flash_Buffer[20];

uint16_t PH_Factor;            //PH校准系数 
uint8_t  Symbol = 0;               //符号系数 

extern uint16_t capture ;

//冒泡排序算法 
void BubbleSort( float score[] , uint8_t length )
{
	uint8_t i , j ;
	float temp ;
	for( i =0 ; i < length - 1 ; i++)
        {
            for(j = 0 ; j <  length - 1-i ; j++ )
            {
                if(score[j] < score[j+1])
                {
                    temp = score[j];
                    score[j] = score[j+1];
                    score[j+1] = temp;
                }
            }
        }
}

uint16_t last_PH100 = 0 ;
/***********************************
  * @brief  获取PH值
  * @param  温度值
  * @retval PH值
************************************/
uint16_t Get_PH( uint16_t temp )
{
   uint8_t i , j;
	float PH_Value = 0 ;
	float PH_return =0 ;
	uint16_t PH100 = 0 ;
	uint16_t PH1000 = 0 ;
	float PH_cal = 0 ;
	float PH_Sum[4] = {0 , 0 , 0 , 0} ;
	float PH_Temp = 0 ;
	uint16_t PH_adc = 0 ;
	uint16_t VREF_adc = 0 ;
	float PH_Div = 0 ;
	
	PH_Temp = (float)temp / 10.0 ;
	for ( i = 0 ; i < 4 ; i++)      //连续取4组
	{
		for(j = 0 ; j <10 ; j++)       //每组取10次
		{
			//TEST_Toggle();//测试用
			PH_adc=PH_ADC_Vaulue();      //获得ph原始adc值 
			VREF_adc = VREF_ADC_Vaulue(); //获得vref原始adc值 
			
			if(PH_adc >= VREF_adc)       // 防止产生负数(uint类型负数以补码形式表示) 从adc转化为ph值
			{
				PH_Div =  (float)(PH_adc-VREF_adc)/(float)VREF_adc * 1240 ;
				PH_Value = 7 + (PH_Div/59.16)*((float)298/(273+PH_Temp));
			}
			else
			{
				PH_Div =  (float)(VREF_adc - PH_adc)/(float)VREF_adc * 1240 ;
				PH_Value = 7 - (PH_Div/59.16)*((float)298/(273+PH_Temp));
			}
			PH_Sum[i] += PH_Value;     // 累加10次
			delay_us(1880);            //延时操作 加上adc转换时间为2ms获取一次
			if( capture != 0 )         //若其中有获取到命令，则直接跳出，返回错误FFFF
			{
			return 0xFFFF;
			}	
		}
	}
	
	BubbleSort(PH_Sum ,4);        //冒泡排序算法
		
	PH_cal = (PH_Sum[1] + PH_Sum[2]) /20.0 ;    // 取中间的两个测量十次的结果再取平均值 
		
	if(Symbol == 0x26)          //补偿正系数 
  {
		PH_return = PH_cal + (float)(PH_Factor/100.0);
  }
  else if(Symbol == 0x62)         //补偿负系数 
  {
		PH_return = PH_cal - (float)(PH_Factor/100.0);
  }
  else
  {
		PH_return = PH_cal;
  }
		
	PH1000 = (uint16_t)(PH_return*1000);  //对小数点后第三位四舍五入
	
	if( (PH1000 % 1000) >= 5) //若这一位大于5，则在未乘1000的原结果上+0.01
	{
		PH_return = PH_return + 0.01 ;
	}
	
	if( PH_return >= 0 )                   //返回值为ph*100（产生2浮点数传输）
		PH100 = (uint16_t)(PH_return*100);
  else
		PH100 = 0;
	
  if( PH100 >= 1400 )
		PH100 = 1400;
	
	if(last_PH100 >= PH100)   //若ph值波动在正负0.03内，则返回值仍未上一次的ph值
	{
		if( last_PH100 - PH100 < 3 )
			PH100 = last_PH100 ;
	}
	else
	{
		if( PH100 - last_PH100 < 3 )
			PH100 = last_PH100 ;      
	}
	
	
	last_PH100 = PH100 ;
	
	return PH100;
}

