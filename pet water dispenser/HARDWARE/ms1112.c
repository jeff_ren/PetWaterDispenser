#include "ms1112.h"
#include "myiic.h"

uint8_t ms1112_read_statereg()
{ 
	uint8_t statereg[3];
  I2C_Start();
	I2C_Write_Byte(ms_deviceid_r);
  statereg[0]=I2C_Read_Byte(0);
  statereg[1]=I2C_Read_Byte(0);
  statereg[2]=I2C_Read_Byte(0);
  I2C_Stop();
	
	return statereg[2];
}

uint16_t ms1112_read_data(uint16_t data)
{
  I2C_Start();
	I2C_Write_Byte(ms_deviceid_r);
  data=I2C_Read_Byte(0)<<8;
  data+=I2C_Read_Byte(0);
  I2C_Stop();
	
	return data;
}

