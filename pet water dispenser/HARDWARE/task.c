#include "task.h"
#include "check.h"
#include "pulse.h"
#include "delay.h"
#include "flash.h"
#include "ntc.h"
#include "ph.h"


uint8_t ACK_Da[120];
uint16_t Cablib_Num = 0;                                 
uint8_t  Cablib_Flag = 0,Cablib_Success = 0;             
uint16_t PH_Calib_Value = 0;

extern uint16_t PH_Factor;            //PH校准系数
extern uint8_t  Symbol;               //符号系数
extern uint8_t sen_table[100];
extern uint8_t Device_Message[14];         
extern uint8_t Ver_Message[2];      
extern uint16_t PH_Calib_Value;
extern uint8_t Flash_Buffer[20];
extern uint8_t T_Symbol,T_Value;

/***********************************
  * @brief  反馈
  * @param  命令类型 ， 数据 ， 长度
  * @retval none
************************************/
void ACK_Data(uint16_t Cmd_Type,uint8_t Data[],uint16_t len)
{   
  uint16_t k;uint16_t chksummcu = 0;
  ACK_Da[2] = Cmd_Type%256;
  ACK_Da[3] = Cmd_Type/256;
		
  ACK_Da[4] = len%256;
  ACK_Da[5] = len/256;
		
  ACK_Da[6] = 0x00;
  ACK_Da[7] = 0x00;
		
  for(k=0;k<len;k++)
  {
    ACK_Da[8+k] = Data[k];
  }
  
  chksummcu = SUM_CHECK(ACK_Da+2,len+6);
  
  ACK_Da[0] = chksummcu %256;
  ACK_Da[1] = chksummcu /256;
  
  delay_ms(10);
  Pulse_SendData(ACK_Da,len+8);
}

/***********************************
  * @brief  传感器参数查询
  * @param  none
  * @retval none
************************************/
void Parameter_Query(void)
{
  uint8_t j;uint8_t Auth_Data[50];
  if((sen_table[10] == 0xA2 || sen_table[10] == 0xFF) && (sen_table[13] == 0x0C))
  {
    Auth_Data[0] = 0x08;
    Auth_Data[1] = sen_table[9];
    Auth_Data[2] = 0xA2;
    Auth_Data[3] = 0x00;
    Auth_Data[4] = 0x00;
    Auth_Data[5] = sen_table[13];
    for(j=0;j<14;j++)
    Auth_Data[j+6] = Device_Message[j];
    for(j=0;j<2;j++)
    Auth_Data[j+20] = Ver_Message[j];
    ACK_Data(0x0b03,Auth_Data,22);
  }
  else
  {
    for(j=0;j<6;j++)
    {
      Auth_Data[j] = sen_table[j+8];
    }
    for(j=0;j<16;j++)
    {
      Auth_Data[j+6] = 0x00;
    }
    ACK_Data(0x0b03,Auth_Data,22);
  }
}

extern float NTC1_Temp , NTC2_Temp ;
/***********************************
  * @brief  传感器数据查询
  * @param  none
  * @retval none
************************************/
void Data_Query(void)
{
  uint8_t j;uint8_t Auth_Data[50];
  uint16_t Temp_Table = 0;
  uint16_t PH_Table = 0;
  if(sen_table[10] == 0xA2 || sen_table[10] == 0xFF)
  {
    Temp_Table = Temp_Get(); // Temp_Get返回的是有符号位的类型 直接强制转化为无符号位的类型 可以直接实现补码形式表示负数
    PH_Table =  Get_PH(Temp_Table);   //获取PH值函数，返回值为ph*100（例如：ph=6.56，此函数返回656）
    
	  if ( (NTC1_Temp - NTC2_Temp) >= 3 || (NTC2_Temp - NTC1_Temp) >= 3 )  //如果两个温度计相差温度超过2度 则返回值为FFFF
			Temp_Table = 0xFFFF;
		
    Auth_Data[0] = 0x09;
    Auth_Data[1] = sen_table[9];
    Auth_Data[2] = 0xA2;
    
    Auth_Data[3] = 0x01;
    Auth_Data[4] = (uint8_t)(Temp_Table % 256); //小端模式传输
    Auth_Data[5] = (uint8_t)(Temp_Table / 256);
    
    Auth_Data[6] = 0x04;
    Auth_Data[7] = (uint8_t)(PH_Table % 256);
    Auth_Data[8] = (uint8_t)(PH_Table / 256);
    
    Auth_Data[9] = Ver_Message[1];                //版本
   // Auth_Data[10] = PH_Table1 % 256;
    //Auth_Data[11] = PH_Table1 / 256;
    
    ACK_Data(0x0b03,Auth_Data,12);
  }
  else
  {
    for(j=0;j<3;j++)
    Auth_Data[j] = sen_table[j+8];
    for(j=0;j<9;j++)
    Auth_Data[j+3] = 0x00;
    ACK_Data(0x0b03,Auth_Data,12);
  }
}
uint16_t last_Symbol;
uint16_t last_PH_Factor;
extern uint16_t Differ_Value;
/***********************************
  * @brief  传感器校准（温度、ph标准液）
  * @param  none
  * @retval none
************************************/
void Correct_Process(void)
{
  uint8_t h;
  uint8_t Auth_Data[10];uint16_t Temp_Table,Temp_Coeff;
	uint16_t temp ;
	uint16_t Cablib_Value;
 PH_Calib_Value = sen_table[13] + (uint16_t)sen_table[14]*256 ;
  for(h=0;h<5;h++)
  {
    Auth_Data[h] = sen_table[h+8];
  }
  if((sen_table[10] == 0xA2)  && (sen_table[12] == 0x01 || sen_table[12] == 0x05))
  {
    if(sen_table[12] == 0x01)                                    //对温度校准 （暂时不用）
    {
      T_Symbol = 0;
      Temp_Table = Temp_Get();                                   //当前温度
      Temp_Coeff = sen_table[13] + (uint16_t)sen_table[14]*256;  //校准温度
      if(Temp_Coeff >= Temp_Table)
      {
        T_Symbol = 0x26;
        T_Value = (uint8_t)(Temp_Coeff - Temp_Table);
      }
      else 
      {
        T_Symbol = 0x62;
        T_Value = (uint8_t)(Temp_Table - Temp_Coeff);
      } 
			Flash_Buffer[18] = T_Symbol;
			Flash_Buffer[19] = T_Value;
			Write_Data(0,Flash_Buffer);
      delay_ms(10);
      Auth_Data[0] = 0x0C;
      Auth_Data[1] = sen_table[9];
      Auth_Data[2] = 0xA2;
      Auth_Data[3] = sen_table[11];
      Auth_Data[4] = sen_table[12];
      Auth_Data[5] = 0xAA;
      ACK_Data(0x0b04,Auth_Data,6);
    } 
    else
    {
      Auth_Data[0] = 0x0C;
      Auth_Data[1] = sen_table[9];
      Auth_Data[2] = 0xA2;
      Auth_Data[3] = sen_table[11];
      Auth_Data[4] = sen_table[12];
      Auth_Data[5] = 0x00;
      ACK_Data(0x0b04,Auth_Data,6);
    }
  }
  if(sen_table[10] == 0xA2 && sen_table[12] == 0x04 && PH_Calib_Value > 0 && PH_Calib_Value <= 1400) //ph值校准
  {
    if(sen_table[15] == 0x01 && sen_table[16] == 0x00)  //取消校准
    {
      PH_Factor = last_PH_Factor;
			Symbol = last_Symbol;
			Flash_Buffer[15] = Symbol;   //恢复到未校准之前的值
			Flash_Buffer[16] = PH_Factor%256;
			Flash_Buffer[17] = PH_Factor/256;
			Write_Data(0,Flash_Buffer);
			delay_ms(10);
			Cablib_Flag = 0;Cablib_Success = 0;
      Auth_Data[5] = 0xFF;
    }
    else if(sen_table[15] == 0x00 && sen_table[16] == 0x00)//开始校准或校准反馈 
    {
			Cablib_Flag = 1 ;    //校准标志位置1
      if(Cablib_Success == 0x01)
      {
        Auth_Data[5] = 0xAA;       //校准成功
				Cablib_Success = 0;
				Cablib_Flag = 0 ;
      }
      else if(Cablib_Success == 0x00)
      {
        Auth_Data[5] = 0x01;      //正在校准中
      }
      else
      {
        Auth_Data[5] = 0x00;      //校准失败
				Cablib_Success = 0;
				Cablib_Flag = 0 ;
      }
    }
  }
  else
  {
    Cablib_Flag = 0;
		Cablib_Success = 0;
    Auth_Data[5] = 0x00;       //校准失败
  }
  ACK_Data(0x0b04,Auth_Data,6);
}

/***********************************
  * @brief  传感器日期信息设置
  * @param  none
  * @retval none
************************************/
void Set_Message_Process(void)
{
  uint8_t j;uint8_t Auth_Data[6];
  if((sen_table[10] == 0xA2 || sen_table[10] == 0xFF) && (sen_table[11] >= 0x01))
  {
    if(sen_table[11] == 0x01)                  //只设置出厂日期
    {
      for(j=0;j<7;j++)
      Device_Message[j] = sen_table[j+12];
    }
    else if(sen_table[11] == 0x02)          //只设置使用日期
    {
      for(j=0;j<7;j++)
      Device_Message[j+7] = sen_table[j+19];
    }
    else if(sen_table[11] == 0x03)          //既设置出厂日期又输出使用日期
    { 
      for(j=0;j<14;j++)
      Device_Message[j] = sen_table[j+12];
    }
    Device_Write(Device_Message);
    Auth_Data[0] = 0x0F;
    Auth_Data[1] = sen_table[9];
    Auth_Data[2] = 0xA2;       
    Auth_Data[3] = sen_table[11];
    Auth_Data[4] = 0xAA;
    ACK_Data(0x0b04,Auth_Data,5);
  }
  else
  {
    for(j=0;j<4;j++)
    Auth_Data[j] = sen_table[j+8];
    Auth_Data[4] = 0x00;
    ACK_Data(0x0b03,Auth_Data,5);
  }
}

/***********************************
  * @brief  校准处理函数
  * @param  none
  * @retval none
************************************/
void Calib_Dispose(void)
{
	uint8_t h,a,b;
	uint16_t temp ;
	uint16_t Cablib_Value;
	if( Cablib_Flag == 1 )  //若校准标志位为1，开始校准
	{
		last_Symbol = Read_Data(15);
		a = Read_Data(16);
		b = Read_Data(17);
		last_PH_Factor = a + b*256 ; //记录校准前的值，若取消校准，需要返回到此值
		
		temp = Temp_Get();
		Symbol = 0;  
		PH_Factor = 0 ;
		Cablib_Value = Get_PH(temp);                        //获取ph校准系数为0时候的原始数据
		
		if(PH_Calib_Value >= Cablib_Value)   //与标准值进行比较
		{
			Differ_Value = PH_Calib_Value - Cablib_Value;  //获得差值
			if(Differ_Value <= 700)  
			{  
				PH_Factor = Differ_Value;  //记录校准值
				Symbol = 0x26;
				Flash_Buffer[15] = Symbol;
				Flash_Buffer[16] = PH_Factor%256;
				Flash_Buffer[17] = PH_Factor/256;
				Write_Data(0,Flash_Buffer);
				delay_ms(10);
				Cablib_Success = 0x01;               //成功
			}
			else
			{
				Cablib_Success = 0x02;               //失败
			}
		}
		else if(PH_Calib_Value < Cablib_Value)
		{
			Differ_Value = Cablib_Value - PH_Calib_Value;
			if(Differ_Value <= 700)
			{ 
				PH_Factor = Differ_Value;
				Symbol = 0x62;
				Flash_Buffer[15] = Symbol;
				Flash_Buffer[16] = PH_Factor%256;
				Flash_Buffer[17] = PH_Factor/256;
				Write_Data(0,Flash_Buffer);
				delay_ms(10);
				Cablib_Success = 0x01;               //成功
			}
			else
			{
					Cablib_Success = 0x02;               //失败
			}
		}
	Cablib_Flag = 0;
	}
}
/*********************************************/
