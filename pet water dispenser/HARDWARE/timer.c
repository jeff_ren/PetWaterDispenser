#include "include.h"

uint16_t key_count;
uint8_t key_hit_count;
extern uint8_t key_flag;
extern uint8_t KeyPress;
extern uint8_t key_hit ;
extern DATA_BUFFER data_buffer ;
/***********************************
  * @brief  定时器2初始化
  * @param  none
  * @retval none
************************************/
void TIM2_Int_Init(uint16_t arr,uint16_t psc)
{
		TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	  NVIC_InitTypeDef NVIC_InitStructure;
	
	  /* TIM3 clock enable */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	//???TIM3???
	TIM_TimeBaseStructure.TIM_Period = arr; //???????????????????????????	
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //??????TIMx???????????
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //??????:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM??????
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //??????????TIMx???????
 
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE ); //?????TIM3??,??????

	//?????NVIC??
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;  //TIM3??
	NVIC_InitStructure.NVIC_IRQChannelPriority = 1;  //?????0?
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ?????
	NVIC_Init(&NVIC_InitStructure);  //???NVIC???


	TIM_Cmd(TIM2, ENABLE);  //??TIMx	

}

uint8_t pump_lack_count = 0 ;
uint8_t pump_led_count = 0 ;
uint16_t pump_work_count = 0 ;
uint8_t fakeTDS_count = 0 ;
extern uint8_t pump_lack_flag ;
extern uint8_t fakeTDS_count_flag ;
extern uint8_t PutInWater_Flag ;
/***********************************
  * @brief  定时器2中断函数
  * @param  none
  * @retval none
************************************/
void TIM2_IRQHandler(void)  
{
	 
	
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)  //??TIM2????????
		{
			TIM_ClearITPendingBit(TIM2, TIM_IT_Update);  //??TIMx?????? 
			
			if( key_flag == 0 && KeyPress == 1) //5s????
			{
				key_count++ ;
				if( key_count >= 50 ) // 100ms * 50 = 5s 
				{
					key_flag = 1 ; 
					key_count = 0 ;
				}
			}
			//连续两次按键时间间隔
			if( key_hit == 1)
			{
				key_hit_count ++ ;
			}
			
			//水泵缺水检测
			if( GPIO_ReadInputDataBit( GPIOB , GPIO_Pin_4 )  == RESET )
			{
				if( pump_lack_count >= 9 && pump_lack_count <= 11 )
				{
					pump_lack_flag = 1 ;
					pump_lack_count = 0 ;
				}
				pump_lack_count ++ ;
			}
			else if ( GPIO_ReadInputDataBit( GPIOB , GPIO_Pin_4 )  == SET )
			{
				if( pump_lack_count > 12 )
				{
					pump_lack_count = 0 ;
					pump_lack_flag = 0 ;
					BLUE_LAMP(OFF);
				}
				pump_lack_count ++ ;
			}
			
			if( pump_lack_flag == 1 )  // 缺水 500ms led 亮 500ms 灭
			{
				pump_led_count ++ ;
				if ( pump_led_count == 5 )
					BLUE_LAMP(ON);
				else if (  pump_led_count == 10 )
				{
					BLUE_LAMP(OFF);
					pump_led_count = 0 ;
				}
			}
			
			if(data_buffer.pump_work_mode == SMART_MODE)
			{
				pump_work_count ++ ;
			}
			
			if( fakeTDS_count_flag == 1 )
			{
				if( fakeTDS_count == 20 )
				{
					fakeTDS_count = 0 ;
					PutInWater_Flag = 0 ;
					fakeTDS_count_flag = 0;
					
				}					// 延时3s 
				fakeTDS_count ++ ;
			}
			
	  }
}
