#ifndef __TDS_H
#define __TDS_H

#include "stm32f0xx.h"

#define IO1_1                         GPIO_SetBits(GPIOA, 5)
#define IO1_0                         GPIO_ResetBits(GPIOA, 5)
#define IO2_1                         GPIO_SetBits(GPIOA, 4)
#define IO2_0                         GPIO_ResetBits(GPIOA, 4)

//#define LED_TURN                      GPIOB->ODR ^= 0x02

void TDS_Init(void);
float TDS_Single_Measure(void);
float TDS_Multi_Measure( uint8_t number );
uint16_t TDS_RxDispose(void);
void TDS_Calibration(void);
#endif
