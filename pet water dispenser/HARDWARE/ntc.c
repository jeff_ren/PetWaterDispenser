#include "ntc.h"
#include "adc.h"
#include <math.h>
//温度补偿正负系数，温度正负系数
uint8_t T_Symbol,T_Value;
float T_Temp1;
/***********************************
  * @brief  二分法查表温度计算
  * @param  none
  * @retval 温度获取值
************************************/ 
float Dichotomy_Temp(float Temp)
{
	uint8_t min , max , index , i;
	float NTC_Temp ; 
	min = 0 ;
	max = 179 ;
	i = 0 ;
	
	if ( Temp >= Temp_Tab[min]  || Temp <= Temp_Tab[max] ){
		NTC_Temp = 1 / ( ( 2.3026 * ( log10(Temp/10000) ) / 3450 ) + ( 1/298.15 ) ) - 273.15; //低于-55°高于125°采用公式计算
		return NTC_Temp ;
	}
	
	while ( min < max )  //二分法计找到温度区间
	{
		index = (max + min) / 2 ;
		
		if ( Temp == Temp_Tab[index] )
			break ;
		
		if ( Temp < Temp_Tab[index] && Temp > Temp_Tab[index+1])
			break ; 
		
		if(Temp > Temp_Tab[index]) 
			max = index ;                    
    else
			min = index ;
		
   if(i++ > 179) break ; 
	}
	if( min > max ) return 0 ;
	
	NTC_Temp = ( 1.0 / ( (float)Temp_Tab[index+1] - (float)Temp_Tab[index] ) ) * ( Temp - Temp_Tab[index] ) + index - 55  ; // 一度之内采用线性取值
		
	return NTC_Temp;
	
}

float NTC1_Temp , NTC2_Temp ;
/***********************************
  * @brief  温度值计算子程序
  * @param  none
  * @retval 温度获取值(摄氏度)
************************************/ 
float Temp_Calculate(float Temp_Value)
{
  float NTC_Temp;
	
	NTC_Temp = Dichotomy_Temp(Temp_Value); // 二分查表法获得温度值
	
  return NTC_Temp;
}

/***********************************
  * @brief  温度值获取
  * @param  none
  * @retval 温度获取值
************************************/ 
int16_t Temp_Get(void)
{
  float Temp_Value;
  float T_Temp;
  Temp_Multi_Measure( 10 );  //十次测量取平均值
  T_Temp = Temp_Calculate( T_Temp1 );    //计算温度值

  Temp_Value = (uint16_t)(T_Temp*10);

	
  return Temp_Value;
}

/***********************************
  * @brief  NTC 多次模式测量
  * @param  测量次数
  * @retval 温度获取值
************************************/
void	Temp_Multi_Measure( uint8_t number )
{
	uint16_t Temp_Single_Value1 ;
	uint16_t Temp1_Origin ;	
//	float V_Temp1 ;
	uint16_t Temp_Value_Sum1 = 0 ;
	uint8_t i ;

	for( i = 0 ; i < number ; i++ )
	{
		Temp_Single_Value1 = NTC1_ADC_Vaulue(); // ntc1
		Temp_Value_Sum1 = Temp_Value_Sum1 + Temp_Single_Value1 ; // 采集多次
	}
	Temp1_Origin = Temp_Value_Sum1 / number ; //取平均值
	
	T_Temp1 = (float)(10000 * Temp1_Origin) / (4095 - Temp1_Origin );  // 转换为电阻值
}
