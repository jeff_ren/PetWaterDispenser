#include "check.h"

/***********************************
  * @brief  和校验
  * @param  校验组数
  * @retval 校验值
************************************/ 
uint16_t SUM_CHECK(uint8_t *puchMsg, uint16_t usDataLen)
{
  uint16_t csum=0xC0AD;
  while(usDataLen)
  {
    csum +=  *puchMsg;
    puchMsg++;
    usDataLen--;
  }
  return csum;
}
