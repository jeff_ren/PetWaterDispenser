#ifndef __PH_H
#define __PH_H

#include "stm32f0xx.h"

void PH_Init(void);
uint16_t Get_PH(uint16_t temp);
void BubbleSort( float score[] , uint8_t length );
#endif
