#ifndef __KEY_H__
#define __KEY_H__
#include "stm32f0xx.h"

#define GPIO_KEY_CLK    RCC_AHBPeriph_GPIOB
#define GPIO_KEY_PORT   GPIOB
#define GPIO_KEY1_PIN    GPIO_Pin_6
#define GPIO_KEY2_PIN    GPIO_Pin_5
 

#define MAX_KEY         2
#define PRESS_KEY      0x01

#define KEY_ON          0x01
#define KEY_OFF         0x00  	

/*****************************************************************************
函数名称 : KEY_Init
功能描述 : KEY初始化
输入参数 : 无
返回参数 : 无
使用说明 : 无
*****************************************************************************/
void KEY_Init(void);

/*****************************************************************************
函数名称 : Key_Scan
功能描述 : 扫描按键
输入参数 : 无
返回参数 : 无
使用说明 : 无
*****************************************************************************/
void Key_Scan(void);
static uint8_t Get_Key1(void) ;
static uint8_t Get_Key2(void) ;
void Key_state_init(void);
void Key_RGBW_init(void);

#endif

