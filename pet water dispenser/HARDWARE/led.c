#include "led.h"


/***********************************
  * @brief  LED初始化
  * @param  none
  * @retval none
************************************/
void LED_Init(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
	//管脚配置 PA8,PA9,PB12,PB13,PB14,,PB15

	GPIO_InitStructure.GPIO_Pin= GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;           //输出模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;  //高速
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;       //推挽输出
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
	GPIO_Init(GPIOA,&GPIO_InitStructure);

	
	GPIO_InitStructure.GPIO_Pin= GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOB,&GPIO_InitStructure);

	GPIO_SetBits(GPIOA, GPIO_Pin_8 | GPIO_Pin_9);
	GPIO_SetBits(GPIOB, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
	
}


