#ifndef __ADC_H
#define __ADC_H

#include "stm32f0xx.h"

void ADC_Config(void);
uint16_t NTC1_ADC_Vaulue(void);
uint16_t NTC2_ADC_Vaulue(void);
uint16_t VREF_ADC_Vaulue(void);
uint16_t PH_ADC_Vaulue(void);
#endif
