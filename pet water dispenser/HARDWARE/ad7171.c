#include "ad7171.h"
#include "delay.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>


// UART-based external variables
uint8_t ucTxBufferEmpty  = 0;       // Used to indicate that the UART Tx buffer is empty
uint8_t szTemp[64] = "";            // Used to store string before printing to UART
uint8_t nLen = 0;
uint8_t i = 0;
uint8_t ucWaitForUart = 0;          // Used by calibration routines to wait for user input 


/***********************************
  * @brief  AD7171相关引脚初始化程序
  * @param  none
  * @retval none
************************************/
void AD7171_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
  
  /* 使能GPIOA时钟 */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* 配置AD7171相应引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	

}

/***********************************
  * @brief  Initializing AD7171 to default settings
  * @param  none
  * @retval none
************************************/
void InitializeAD7171(void)
{
	AD7171_PDRST(0);
	delay_us(100); 			 // 100us delay
	AD7171_PDRST(1);
	delay_ms(1);    //1ms delay
}

/***********************************
  * @brief  Function to Read data from AD7171
  * @param  none
  * @retval none
************************************/
void ReadFromAD7171(void)                 
{
	unsigned int ready = 0x000001;	
	unsigned int DataReg = 0x000000;
	unsigned int InputRegister[10];
	unsigned int OutputRegister[10];
	unsigned int StatusBar = 0x00;
	int i=0;
	

	
		AD7171_SCLK(1);
				
		while(ready)              				// Waits untill the DOUT/RDY pin goes low to indicate 
		{                         			  // data is converted and is ready to be read.
			ready = AD7171_DOUTRDY;	
		}
	
		delay_ms(10);
	
		SpiFunction(InputRegister, OutputRegister, NoOfByte);	 			  //SPI read write

		
			for(i=0; i<2; i++)
        {
         		
					DataReg	<<= 8;
					DataReg |= InputRegister[i];
			     	
				}
			 sprintf ( (char*)szTemp, "AD7171 16-bit Data = 0X%x \r\n",DataReg);
		   nLen = strlen((char*)szTemp);
//		   if (nLen <64)
//				 SendString();   串口打印数据，我们没有串口所以不要
		
			 StatusBar = (InputRegister[2] & 0x0000FF);
			 
			 //Display Status Bits through UART
			 sprintf ( (char*)szTemp, "AD7171 8-Bit Status Bar = 0X%x \r\n",StatusBar);
		   nLen = strlen((char*)szTemp);
//		   if (nLen <64)
//				 SendString();  串口打印状态，我们没有串口所以不要
				
		ErrorCheck(InputRegister[2]);    // Performs Error Check in AD7171 Received Data. 'ErrorBit' is set to 1 if any error is present.
		delay_us(100);
		
		do              
		{
			ready = AD7171_DOUTRDY;	
		}	while(ready != 0x01);
			                        
}

/***********************************
  * @brief  Performs Error Check in the received data
  * @param  none
  * @retval none
************************************/
 void ErrorCheck(unsigned int Status)
{
	if(0x20 ==(Status & 0x20)) 
    {                                            	   	 
	      InitializeAD7171();             //Initializes the AD7171 device when the Error bit is set       
																				//or when the pattern bits PAT2,PAT1 and PAT0 are not equal to 101                 
		                                   //or when ID bits ID1 and ID0 are not equal to 01.
				   sprintf ( (char*)szTemp, "Error Present as Error Bit in Status Register is set high \r\n");
		   nLen = strlen((char*)szTemp);
//		   if (nLen <64)
//				 SendString();
	  }
	if (0x05 != (Status & 0x05))
		{
			InitializeAD7171(); 
		    sprintf ( (char*)szTemp, "Error due to mismatch in Pattern Bits \r\n");
		   nLen = strlen((char*)szTemp);
//		   if (nLen <64)
//				 SendString();                                   
	  }
	if (0x08 != (Status & 0x08))
		{
			InitializeAD7171(); 
		    sprintf ( (char*)szTemp, "Error due to mismatch in ID bits of Status Bar \r\n");
		   nLen = strlen((char*)szTemp);
//		   if (nLen <64)
//				 SendString();                                   
	  }
	else if (0x0D ==(Status & 0x0D)) 
		{
			sprintf ( (char*)szTemp, "No Error \r\n");
		   nLen = strlen((char*)szTemp);
//		   if (nLen <64)
//				 SendString();
		}
}
	
//void UARTINIT (void)
//{
//	//Select IO pins for UART.
//	pADI_GP0->GPCON |= 0x3C;                     // Configure P0.1/P0.2 for UART	
//		DioCfgPin(pADI_GP0,PIN1,3);
//	DioCfgPin(pADI_GP0,PIN2,3);
//	UrtCfg(pADI_UART,B9600,COMLCR_WLS_8BITS,0);  // setup baud rate for 115200, 8-bits,stop bits -1 no parity.
//	UrtMod(pADI_UART,COMMCR_DTR,0);              // Data terminal ready, Setup modem bits
//	UrtIntCfg(pADI_UART,COMIEN_ERBFI|COMIEN_ETBEI|COMIEN_ELSI|COMIEN_EDSSI|COMIEN_EDMAT|COMIEN_EDMAR);   // Setup UART IRQ sources
//}


//void UART_Int_Handler ()
//{
//   volatile unsigned char ucCOMSTA0 = 0;
//   volatile unsigned char ucCOMIID0 = 0;

//   ucCOMIID0 = UrtIntSta(pADI_UART);   // Read UART Interrupt ID register
//   if ((ucCOMIID0 & 0x2) == 0x2)       // Transmit buffer empty
//   {
//      ucTxBufferEmpty = 1;
//   }
//   if ((ucCOMIID0 & 0x4) == 0x4)       // Receive byte
//   {
//      ucWaitForUart = 0;
//   }
//}

//void SendString (void)
//{
//   for ( i = 0 ; i < nLen ; i++ )	// loop to send ADC result
//   {
//      ucTxBufferEmpty = 0;
//      UrtTx(pADI_UART,szTemp[i]);
//      while (ucTxBufferEmpty == 0)
//      {
//      }
//   }
//}


/***********************************
  * @brief  Subprogram to Read data from AD7171
  * @param  none
  * @retval none
************************************/
void SpiFunction(unsigned int *InputBuf, unsigned int *OutputBuf, unsigned int NoOfBytes)
{
int i,j;
unsigned int TempInput = 0x00;
//unsigned int TempOutput = 0x00;	
unsigned int ReceivedBit = 0x00;

	for(i=0; i< NoOfBytes; i++)
	{
		//TempOutput = *OutputBuf;
		for(j=0; j<8; j++)
		{
				////////////WRITE DATA/////////////
			
			TempInput<<=1;
			
			AD7171_SCLK(0);
			delay_ms(1);	
			AD7171_SCLK(1);
			delay_ms(1);
			
		//if(0x80==(TempOutput & 0x80))
		//	{
		//	ADuCM360GpioOutput(AD7171_SDIN, PIN_HIGH);
		//	}
		//	else
		//	{
		//		ADuCM360GpioOutput(AD7171_SDIN, PIN_LOW);
		//	}
		//  TempOutput<<=1;	
		//	Delay(1);
		
			////////////WRITE DATA ENDS/////////
			
			
			///////////READ DATA///////
				   
			ReceivedBit = AD7171_DOUTRDY;	
				if(0x1==ReceivedBit)
					{
						TempInput |= 1;
					}
								
			////////////READ DATA ENDS/////////
			
		} 
		*InputBuf = TempInput;
		OutputBuf++;
		InputBuf++;
			
	}
	
	AD7171_SCLK(1);
	delay_ms(1);
	
}



