#ifndef __TASK_H
#define __TASK_H

#include "stm32f0xx.h"

void ACK_Data(uint16_t Cmd_Type,uint8_t Data[],uint16_t len);

void Parameter_Query(void);
void Data_Query(void);
void Set_Message_Process(void);
void Correct_Process(void);
void Calib_Dispose(void);


#endif
