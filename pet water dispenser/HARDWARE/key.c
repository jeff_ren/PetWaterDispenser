#include "include.h"

extern uint8_t connecting_wifi;

//uint8_t wifi_test_mode = 0;                                                  //产测模式

/*****************************************************************************
函数名称 : KEY_Init
功能描述 : KEY初始化
输入参数 : 无
返回参数 : 无
使用说明 : 无
*****************************************************************************/
void KEY_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_AHBPeriphClockCmd(GPIO_KEY_CLK, ENABLE);
  
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 按键初始化
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5|GPIO_Pin_6;  //PB6 KEY1;PB5 KEY2;  
  GPIO_Init(GPIOB, &GPIO_InitStructure);

}

/*****************************************************************************
函数名称 : Get_Key
功能描述 : 读取按键
输入参数 : 无
返回参数 : ReadKey:按键值
使用说明 : 无
*****************************************************************************/
static uint8_t Get_Key1(void) 
{
  uint8_t ReadKey = 0;
  
  if(!GPIO_ReadInputDataBit(GPIO_KEY_PORT,GPIO_KEY1_PIN))
  {
    ReadKey = PRESS_KEY;
  }
  
  return ReadKey;
}

/*****************************************************************************
oˉêy??3? : Get_Key2
1|?ü?èê? : ?áè?°′?ü1
ê?è?2?êy : ?T
·μ??2?êy : ReadKey:°′?ü?μ
ê1ó??μ?÷ : ?T
*****************************************************************************/
static uint8_t Get_Key2(void) 
{
  uint8_t ReadKey = 0;
  
  if(!GPIO_ReadInputDataBit(GPIO_KEY_PORT,GPIO_KEY2_PIN))
  {
    ReadKey = PRESS_KEY;
  }
  
  return ReadKey;
}

uint8_t key_flag = 0 ;
uint8_t KeyPress=0; 
uint8_t key_hit = 0 ;
DATA_BUFFER data_buffer;
extern uint16_t key_count ;
extern uint8_t key_hit_count;
/*****************************************************************************
函数名称 : Key_Scan
功能描述 : 扫描按键
输入参数 : 无
返回参数 : 无
使用说明 : 无
*****************************************************************************/
void Key_Scan(void)
{
  uint8_t Disappears_Num;

	if( Get_Key1() == PRESS_KEY && KeyPress != 1 )
	{
		Disappears_Num = 0 ;
		while(Get_Key1() == PRESS_KEY)  // 去抖
		{	
			delay_ms(2);
			Disappears_Num++;
			if(Disappears_Num >= 10)
			break;
		}
		if(Disappears_Num >= 10 && Get_Key1() == PRESS_KEY) // 长按
		{
			KeyPress = 1;
			
    }
	}
	 else if( key_flag == 1 && Get_Key1() == PRESS_KEY)  //到达5s
			{				
				// wifi 重启代码 还未写
//				quick_flash_flag=1;
//					mcu_reset_wifi();
			}
		else if(Get_Key1() != PRESS_KEY)      //松开按键
		{
				
			if( KeyPress == 1 )
			{  
				 KeyPress = 0 ;
				 key_flag = 0;
				if( key_count < 100 && key_count > 0 ) //未到5s
				{
					key_count = 0 ; //?????¨ê±2?êy
					key_hit = 1 ;
					if( key_hit_count <= 5 ) //500ms?úó?°′á?ò?′?
					{
						key_hit = 0 ;
						key_hit_count = 0 ;
						if(data_buffer.pump_work_mode == NORMAL_MODE)
							data_buffer.pump_work_mode = SMART_MODE ;
						else if (data_buffer.pump_work_mode == SMART_MODE)
							data_buffer.pump_work_mode = NORMAL_MODE ;
					}
				}
			}
			
			if( key_hit_count > 6 ) //°′?ü??°′á?ò?′?
			{
				key_hit_count = 0 ;
				key_hit = 0;
				if(data_buffer.key1_state == ON)
					data_buffer.key1_state = OFF ;
				else if(data_buffer.key1_state == OFF)
					data_buffer.key1_state = ON ;
			}
	 }
		
		if( Get_Key2() == PRESS_KEY && KeyPress != 1 )
	{
		Disappears_Num = 0 ;
		while(Get_Key2() == PRESS_KEY)  // è￥??
		{	
			delay_ms(2);
			Disappears_Num++;
			if(Disappears_Num >= 10)
			break;
		}
		if(Disappears_Num >= 10 && Get_Key2() == PRESS_KEY) // 3¤°′
		{
			KeyPress = 1;
    }
	}
	 else if( key_flag == 1 && Get_Key2() == PRESS_KEY)  //μ?′?5s
			{				
				// ?ü????D? ￡?????ê±??
			}
		else if(Get_Key1() != PRESS_KEY)      //?é?a°′?ü
		{		
			if( KeyPress == 1 )
			{  
				 KeyPress = 0 ;
				 key_flag = 0;
			}
	  }
	
}


void Key_state_init(void)
{


}


