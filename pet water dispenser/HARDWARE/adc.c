#include "adc.h"


#define ADC1_DR_Address    0x40012440

uint16_t  RegularConvData_Tab[1];
/***********************************
  * @brief  ADC初始化
  * @param  none
  * @retval none
************************************/
void ADC_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	DMA_InitTypeDef  DMA_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;
	//时钟配置
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1 , ENABLE);
	// PA1(NTC1_ADC)

	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_1 ;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
  /* ADCs DeInit */  
  ADC_DeInit(ADC1);
	
	DMA_DeInit(DMA1_Channel1);    /* DMA1 Channel1 Config */
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC1_DR_Address;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)RegularConvData_Tab;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = 1;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);
           
  DMA_Cmd(DMA1_Channel1, ENABLE);/* DMA1 Channel1 enable */           
  ADC_DMARequestModeConfig(ADC1, ADC_DMAMode_Circular); /* Enable ADC_DMA */   
  ADC_DMACmd(ADC1, ENABLE); 
	
	/* Initialize ADC structure */
  ADC_StructInit( &ADC_InitStructure);
	
	//ADC配置
	ADC_InitStructure.ADC_Resolution=ADC_Resolution_12b;
	ADC_InitStructure.ADC_ContinuousConvMode=DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge=ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign=ADC_DataAlign_Right;
	ADC_InitStructure.ADC_ScanDirection=ADC_ScanDirection_Upward;
	ADC_Init(ADC1,&ADC_InitStructure);
	
	ADC_ChannelConfig(ADC1, ADC_Channel_1 , ADC_SampleTime_55_5Cycles); //通道1选择
	ADC_GetCalibrationFactor(ADC1);  //ADC校准        
	ADC_Cmd(ADC1,ENABLE);            //ADC使能
}




/***********************************
  * @brief  NTC1原始ADC数据获取
  * @param  none
  * @retval ADC采集到的NTC原始数据
************************************/
uint16_t NTC1_ADC_Vaulue(void)
{
		uint16_t value = 0 ;
	
	/* Wait the ADRDY flag */
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY)); 
	
  /* ADC1 regular Software Start Conv */ 
  ADC_StartOfConversion(ADC1);
	
 /* Test DMA1 TC flag */
  while((DMA_GetFlagStatus(DMA1_FLAG_TC1)) == RESET ); 
    
  /* Clear DMA TC flag */
  DMA_ClearFlag(DMA1_FLAG_TC1);
	
	ADC_StopOfConversion(ADC1);
	
	value=RegularConvData_Tab[0];
	
	return value ; 
}

