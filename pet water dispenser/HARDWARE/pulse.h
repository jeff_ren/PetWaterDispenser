#ifndef __PULSE_H
#define __PULSE_H

#include "stm32f0xx.h"

#define Pluse_1    GPIO_SetBits(GPIOA, GPIO_Pin_9)
#define Pluse_0    GPIO_ResetBits(GPIOA, GPIO_Pin_9)

void Pulse_In_Config(void);
void Pulse_Out_Config(void);
void Pulse_Start(void) ;
void Pulse_Stop(void) ;
void Send_Data0(void) ;
void Send_Data1(void) ;
void Pulse_SendByte(uint8_t Byte) ;
void Pulse_RxDispose(void);
void Pulse_SendData(uint8_t pluse_data[],uint8_t len);
void Data_RxDispose(void);

#endif
