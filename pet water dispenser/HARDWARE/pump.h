#ifndef __PUMP_H
#define	__PUMP_H


#include "stm32f0xx.h"

#define NORMAL_MODE      0x01
#define SMART_MODE       0x02

#define Pump_Work(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_15):GPIO_ResetBits(GPIOA,GPIO_Pin_15)) 

void Pump_Init(void);
void Pump_Dispose(void);
	
#endif 
