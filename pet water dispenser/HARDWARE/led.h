#ifndef __LED_H
#define	__LED_H


#include "stm32f0xx.h"

#define ON            0x00
#define OFF           0x01

#define TDS_B_LED(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_9):GPIO_ResetBits(GPIOA,GPIO_Pin_9)) 
#define TDS_R_LED(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_8):GPIO_ResetBits(GPIOA,GPIO_Pin_8)) 
#define TDS_G_LED(n)  (n?GPIO_SetBits(GPIOB,GPIO_Pin_15):GPIO_ResetBits(GPIOB,GPIO_Pin_15)) 

#define KEY1_LED(n)  (n?GPIO_SetBits(GPIOB,GPIO_Pin_12):GPIO_ResetBits(GPIOB,GPIO_Pin_12)) 
#define KEY2_LED(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_8):GPIO_ResetBits(GPIOA,GPIO_Pin_8)) 

#define PUMP_LED(n)  (n?GPIO_SetBits(GPIOB,GPIO_Pin_14):GPIO_ResetBits(GPIOB,GPIO_Pin_14)) 

#define BLUE_LAMP(n)  (n?GPIO_SetBits(GPIOB,GPIO_Pin_13):GPIO_ResetBits(GPIOB,GPIO_Pin_13)) 

#define WIFI_LED(n)  (n?GPIO_SetBits(GPIOB,GPIO_Pin_12):GPIO_ResetBits(GPIOB,GPIO_Pin_12)) 


void LED_Init(void);
	
#endif 

