#ifndef __I2C_H
#define __I2C_H

#include "stm32f0xx.h"

#define Deveice_Addr    0x48 // 01001000 device��ַΪ1001000

void I2C_Config(void);
uint32_t I2C_Read(uint8_t DevAddr);
void  I2C_Write(uint8_t DevAddr, uint8_t data);
uint8_t I2C_ReadReg(uint8_t DevAddr);
uint16_t I2C_ReadData(uint8_t DevAddr);
#endif
