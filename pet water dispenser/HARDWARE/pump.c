#include "pump.h"
#include "include.h"

/***********************************
  * @brief  水泵初始化
  * @param  none
  * @retval none
************************************/
void Pump_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	//管脚配置 PA15 水泵控制管脚
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;           //输出模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;  //高速
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;       //推挽输出
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
	GPIO_Init(GPIOA,&GPIO_InitStructure);

	GPIO_ResetBits(GPIOA, GPIO_Pin_15);
	
	//管脚配置 PB4 水泵数据管脚
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IN;           //输入模式
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB,&GPIO_InitStructure);

}

uint8_t pump_lack_flag ;
uint8_t pump_work_flag ;
uint8_t pump_open_flag ;
extern DATA_BUFFER data_buffer ;
extern uint16_t pump_work_count ;
/***********************************
  * @brief  水泵处理函数
  * @param  none
  * @retval none
************************************/
void Pump_Dispose(void)
{
	if( pump_lack_flag == 1 ) //缺水上报操作
	{
		//上报一次
	}
	else
	{
		//上报一次
		//直接在回传函数里操作
	}

	if(data_buffer.pump_work_mode == NORMAL_MODE) //水泵工作模式
	{
		Pump_Work(1);
		pump_open_flag = 0 ;
		PUMP_LED(OFF);
	}
	else if(data_buffer.pump_work_mode == SMART_MODE)
	{
		//智能模式
		/*
		PUMP_LED(ON);
		if(RTC_TIME > 0x0600 && RTC_TIME < 0x1800) //白天
		{
			if( pump_work_count > 1200 && pump_open_flag == 0) //白天开2min
			{
				Pump_Work(OFF);
				pump_work_count = 0 ;
				pump_open_flag = 1 ;
			}
			else if( pump_work_count > 600 && pump_open_flag == 1) //白天关1min
			{
				Pump_Work(ON);
				pump_work_count = 0 ;
				pump_open_flag = 0 ;
			}
		}
		else
		{
			if( pump_work_count > 3000 && pump_open_flag == 0)    //晚上开5min
			{
				Pump_Work(OFF);
				pump_work_count = 0 ;
				pump_open_flag = 1 ;
			}
			else if( pump_work_count > 33000 && pump_open_flag == 1)  //晚上关55min
			{
				Pump_Work(ON);
				pump_work_count = 0 ;
				pump_open_flag = 0 ;
			}
		}
		*/
	}
		
}
