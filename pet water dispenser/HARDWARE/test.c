#include "test.h"
#include "ph.h"
#include "flash.h"
#include "delay.h"
#include "ntc.h"

#define PH_Calib_Value   700

//test only use use

void test(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	//管脚配置 PA9

	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;           //输出模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;  //高速
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;       //推挽输出
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
	GPIO_Init(GPIOA,&GPIO_InitStructure);

	GPIO_ResetBits(GPIOA, GPIO_Pin_10);
}

uint8_t test_index = 0 ;
void TEST_Toggle(void)
{
	if( test_index == 0 )
	{
		GPIO_SetBits(GPIOA, GPIO_Pin_10);
		test_index = 1 ;
	}
	else
	{
		GPIO_ResetBits(GPIOA, GPIO_Pin_10);
		test_index = 0 ;
	}
}
/***********************************
  * @brief  test初始化（外部中断）
  * @param  none
  * @retval none
************************************/
void TEST_Init(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	GPIO_InitTypeDef   GPIO_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	
  /* Enable GPIOA clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* Configure PA10 pin as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  /* Connect EXTI0 Line to PA0 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource10);

  /* Configure EXTI10 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line10;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI0 Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/***********************************
  * @brief  外部中断函数中断
  * @param  none
  * @retval none
************************************/
void EXTI4_15_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line10)!=RESET)
	{
		Test_Calib();
	}

	EXTI_ClearFlag(EXTI_Line10);
}

extern uint16_t PH_Factor;            //PH校准系数
extern uint8_t  Symbol;               //符号系数
extern uint16_t Cablib_Value1,Cablib_Value2;
uint16_t	Temp_Table_Cab ;
extern uint16_t Differ_Value;
extern uint8_t Flash_Buffer[30];
/***********************************
  * @brief  TEST函数（校准函数）
  * @param  none
  * @retval none
************************************/
void Test_Calib(void) // 增加去抖的延时操作
{
	uint16_t temp ;
	uint16_t Cablib_Value;
	 PH_Factor = 0;                       //校准系数清零
   temp = Temp_Get();
	 Cablib_Value = Get_PH(temp);
   Symbol = 0;                          //获取原始数据
   if(PH_Calib_Value >= Cablib_Value)
   {
     Differ_Value = PH_Calib_Value - Cablib_Value;
     if(Differ_Value <= 700)
     {  
        PH_Factor = Differ_Value;
        Symbol = 0x26;
			  Flash_Buffer[15] = Symbol;
			  Flash_Buffer[16] = PH_Factor%256;
				Flash_Buffer[17] = PH_Factor/256;
				Write_Data(0,Flash_Buffer);
        delay_ms(10);
      }
      else
      {
     //   Cablib_Success = 0x02;               //失败
      }
     }
     else if(PH_Calib_Value < Cablib_Value)
     {
			  Differ_Value = Cablib_Value - PH_Calib_Value;
        if(Differ_Value <= 700)
        { 
          PH_Factor = Differ_Value;
          Symbol = 0x62;
					Flash_Buffer[15] = Symbol;
					Flash_Buffer[16] = PH_Factor%256;
					Flash_Buffer[17] = PH_Factor/256;
					Write_Data(0,Flash_Buffer);
          delay_ms(10);
        }
        else
        {
        //   Cablib_Success = 0x02;               //失败
         }
       }
}

