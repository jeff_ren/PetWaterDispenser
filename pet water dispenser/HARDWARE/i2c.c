#include "i2c.h"
#include "stm32f0xx_i2c.h"

/***********************************
  * @brief  I2C 初始化
  * @param  none
  * @retval none
************************************/ 
void I2C_Config(void)
{
	I2C_InitTypeDef I2C_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	/** I2C1 GPIO Configuration			 
	PA9	   ------> I2C1_SCL		 
	PA10	 ------> I2C1_SDA	*/	
	/*Enable or disable the AHB peripheral clock */	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);	/*Configure GPIO pin */
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9|GPIO_Pin_10;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;	
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/*Configure GPIO pin alternate function */	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);

	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;	
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_DigitalFilter = 0x00;
	I2C_InitStructure.I2C_OwnAddress1=0x00;
	I2C_InitStructure.I2C_Timing =0x10950D21;//400Khz 48m sysclk
	I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
	
	I2C_Init(I2C1, &I2C_InitStructure);	
	I2C_Cmd(I2C1, ENABLE);
	
}

/***********************************
  * @brief  I2C 数据写入（只用于配置寄存器）
  * @param  器件地址，数据
  * @retval none
************************************/ 
void  I2C_Write(uint8_t DevAddr, uint8_t data)
{
	/* While the bus is busy */
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) != RESET);
	
	/* Send Touch address for write */	
	I2C_TransferHandling(I2C1, (DevAddr<<1), 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);
	
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_TXIS)==RESET);
	
	I2C_SendData(I2C1, data);

	/* Send STOP condition */
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF) == RESET);
	
}

/***********************************
  * @brief  I2C 数据读出
  * @param  器件地址
  * @retval none
************************************/ 
uint32_t I2C_Read(uint8_t DevAddr)
{ 
	uint32_t data ;
	uint32_t last_data = 0 ;
	uint8_t i ;
	/* While the bus is busy */
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) != RESET);

	/* Send STRAT condition */  
	I2C_TransferHandling(I2C1, (DevAddr<<1), 3,  I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
	
	/* While there is data to be read */
	for( i = 0 ; i < 3 ; i++ )  
	{	
		while(I2C_GetFlagStatus(I2C1, I2C_FLAG_RXNE) == RESET)

		/* Read a byte from the EEPROM */
		data = last_data + I2C_ReceiveData(I2C1);

		/* Point to the next location where the byte read will be s**ed */
		last_data = data << 8 ;        
	}
	
	/* Enable Acknowledgement to be ready for another reception */
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF) == RESET);
	
	return data ;
}

/***********************************
  * @brief  I2C 寄存器信息读出
  * @param  器件地址
  * @retval none
************************************/ 
uint8_t I2C_ReadReg(uint8_t DevAddr)
{
	uint8_t reg ;
	uint32_t value ;
	value = I2C_Read(DevAddr);
	reg = value % 256 ; // 最后八位是寄存器信息
	return reg;
}

/***********************************
  * @brief  I2C 数据读出
  * @param  器件地址
  * @retval none
************************************/ 
uint16_t I2C_ReadData(uint8_t DevAddr)
{
	uint16_t data ;
	uint32_t value ;
	value = I2C_Read(DevAddr);
	data = value >> 8 ; // 前十六位是寄存器信息
	return data;
}
