#ifndef AD7171_H
#define AD7171_H

#include "stm32f0xx.h"
#define NoOfByte 0x3

#define AD7171_PDRST(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_5):GPIO_ResetBits(GPIOA,GPIO_Pin_5)) //PDRST
#define AD7171_SCLK(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_6):GPIO_ResetBits(GPIOA,GPIO_Pin_6)) //SCLK

#define AD7171_DOUTRDY    GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_7)  //DOUTRDY
//------------------------
// Function declarations
//------------------------
void AD7171_init(void);
void InitializeAD7171(void);
void ReadFromAD7171(void);   
void ErrorCheck(unsigned int StatusReg);
//void UARTINIT (void);
//void UART_Int_Handler (void);
//void SendString (void);
void SpiFunction(unsigned int *InputBuf, unsigned int *OutputBuf, unsigned int NoOfBytes);

#endif

