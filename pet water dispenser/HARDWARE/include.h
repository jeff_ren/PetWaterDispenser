#ifndef _INCLUDE_H_
#define _INCLUDE_H_

#include	<stdio.h>
#include	<math.h>
#include	<stdlib.h>
#include	<string.h>
//#include 	<intrins.h>
#include	<ctype.h>
#include "stm32f0xx.h"

typedef struct 
{
    uint8_t day;
    uint8_t mon;
    uint8_t year;
}TIME;

typedef struct
{
	uint8_t key1_state ;
	uint8_t pump_work_mode;
	TIME    Filter_time ;
}DATA_BUFFER ;

#define         ENABLE_BOOT                             //使能BOOT(固件升级功能)
//(7+1K)+28K+28K
#define         BASIC_FLASH_ADDR                        0x08000000

#ifdef ENABLE_BOOT
  #define         PARA_ADDR                                0x1c00
  #define         OFFSET_PARA1                             0xf800    
  #define         OFFSET_PARA2                             0xfc00    	
#else
  #define         OFFSET_PARA                             0xfc00         
#endif

#define         OFFSET_FIRMWARE_L                       0x2000                  //固件正常运行地址
#define         OFFSET_FIRMWARE_H                       0x9000                  //升级固件存储地址

#define         PARA_ADDR1                               (BASIC_FLASH_ADDR + OFFSET_PARA1)
#define         PARA_ADDR2                               (BASIC_FLASH_ADDR + OFFSET_PARA2)
#define         FIREWARE_ADDR_L                         (BASIC_FLASH_ADDR + OFFSET_FIRMWARE_L)   //
#define         FIREWARE_ADDR_H                         (BASIC_FLASH_ADDR + OFFSET_FIRMWARE_H)   //

#define         FIREWARE_UPDATE_FLAG                           0x55555555

#include "sys.h"
#include "key.h"
#include "delay.h"
#include "adc.h"
#include "timer.h"
#include "flash.h"
#include "hx711.h"
#include "rtc.h"
#include "pump.h"
#include "led.h"
#include "uart.h"
#endif
