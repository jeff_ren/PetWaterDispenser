#include "include.h"

unsigned long HX711_Buffer = 0;
unsigned long Weight_Maopi = 0;
long Weight_Shiwu = 0;
unsigned char flag = 0;
unsigned char Flag_ERROR = 0;

void HX711_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	//时钟配置
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  

	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_12 ;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_11 ;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA,&GPIO_InitStructure);

}
unsigned long HX711_Read(void)	//增益128
{
	unsigned long count; 
	unsigned char i; 
  	HX711_DOUT(1); 
  	HX711_SCK(0); 
  	count=0; 
  	while(HX711_DINPUT); 
	 delay_us(1);
  	for(i=0;i<24;i++)
	{ 
	  	HX711_SCK(1); 
	  	count=count<<1; 
		HX711_SCK(0); 
	  	if(HX711_DINPUT)
			count++; 
			delay_us(1);
	} 
 	HX711_SCK(1); 
  count=count^0x800000;//第25个脉冲下降沿来时，转换数据
	delay_us(1);
	HX711_SCK(0);
  delay_us(1);  
	return(count);
}

//****************************************************
//称重
//****************************************************
void Get_Weight(void)
{
	Weight_Shiwu = HX711_Read();
	Weight_Shiwu = Weight_Shiwu - Weight_Maopi;		//获取净重
	if(Weight_Shiwu > 0)			
	{	
		Weight_Shiwu = (unsigned int)((float)Weight_Shiwu/GapValue); 	//计算实物的实际重量
																		
																		
		if(Weight_Shiwu > 5000)		//超重报警
		{
			Flag_ERROR = 1;	
		}
		else
		{
			Flag_ERROR = 0;
		}
	}
	else
	{
		Weight_Shiwu = 0;
		Flag_ERROR = 1;				//负重报警
	}
	
}

//****************************************************
//去毛皮
//****************************************************
void Get_Maopi(void)
{
	Weight_Maopi = HX711_Read();	
} 
