#ifndef __CRC_H_
#define __CRC_H_


#include "stm32f0xx.h"


uint16_t SUM_CHECK(uint8_t *puchMsg, uint16_t usDataLen);

#endif
