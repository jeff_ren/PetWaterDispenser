#include "adc.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define ADC1_DR_Address                0x40012440
__IO uint16_t RegularConvData_Tab[4];

void ADC1_DMA_Init(void)
{
  ADC_InitTypeDef     ADC_InitStruct;
  DMA_InitTypeDef     DMA_InitStruct;
	GPIO_InitTypeDef    GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	  /* ADC1 DeInit */  
  ADC_DeInit(ADC1);

	/* Enable  GPIOA clock */
	  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  /* ADC1 Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  /* DMA1 clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1 , ENABLE);

  /* Configure PA.01  as analog input */
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStruct);				// PA1,输入时不用设置速率


    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;	
    GPIO_Init(GPIOA, &GPIO_InitStruct);

 
  /* DMA1 Channel1 Config */
  DMA_DeInit(DMA1_Channel1);//复位DMA1_channel1
  DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)ADC1_DR_Address;
  DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)&RegularConvData_Tab;
  DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralSRC;//源地址为外设
  DMA_InitStruct.DMA_BufferSize =1;
  DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;//外设地址寄存器不变
  DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;//内存地址寄存器递增
  DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;//16位
  DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;//16位
  DMA_InitStruct.DMA_Mode = DMA_Mode_Circular;//不停传送
  DMA_InitStruct.DMA_Priority = DMA_Priority_High;
  DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;//
  DMA_Init(DMA1_Channel1, &DMA_InitStruct);
  

	// DMA Transfer Complete Interrupt enable 
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;   //通道
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0; //占先级
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;           //启动
	NVIC_Init(&NVIC_InitStructure);                           //初始化
	 
	DMA_ITConfig(DMA1_Channel1,DMA_IT_TC,ENABLE);
	DMA_ClearITPendingBit(DMA_IT_TC);  
  /* DMA1 Channel1 enable */
  DMA_Cmd(DMA1_Channel1, ENABLE);


	///////////////////////////////////////////////////////////////////////
	TIM_DeInit(TIM1);
	//定时器1控制采样率
	TIM_TimeBaseStructure.TIM_Prescaler= 8-1;
	TIM_TimeBaseStructure.TIM_CounterMode= TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Period= 833;
	TIM_TimeBaseStructure.TIM_ClockDivision= 0;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( TIM1, &TIM_TimeBaseStructure);
	TIM_SelectOutputTrigger( TIM1, TIM_TRGOSource_Update ); 
	 
	TIM_ARRPreloadConfig(TIM1,ENABLE);
	TIM_Cmd( TIM1, ENABLE );
  ////////////////////////////////////////////////////////////////////////
	
//   /* ADC DMA request in circular mode */
  ADC_DMARequestModeConfig(ADC1, ADC_DMAMode_Circular);//103没这个
  
  /* Enable ADC_DMA */
  ADC_DMACmd(ADC1, ENABLE);  //ADC_JitterCmd,改ADC时钟为APB■■■
  
  /* Initialize ADC structure */
  ADC_StructInit(&ADC_InitStruct);//设定默认值■■■ 如果没这句 多路ADC数据会交换?
  
  /* Configure the ADC1 in continous mode withe a resolutuion equal to 12 bits  */
  ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStruct.ADC_ContinuousConvMode = DISABLE;//ENABLE; 
  ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_RisingFalling;//ADC_ExternalTrigConvEdge_None;
	ADC_InitStruct.ADC_ExternalTrigConv=ADC_ExternalTrigConv_T1_TRGO;//■■■■■■■■■■■■■■■■■■
  ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStruct.ADC_ScanDirection = ADC_ScanDirection_Upward;//ADC_ScanDirection_Backward;//？■■■
  ADC_Init(ADC1, &ADC_InitStruct); 
	
	ADC_OverrunModeCmd(ADC1, ENABLE);//使能数据覆盖模式//■■■■■■■■■■■■■■■■■■
 
//   /* Convert the ADC1 temperature sensor  with 55.5 Cycles as sampling time */ 
//   ADC_ChannelConfig(ADC1, ADC_Channel_TempSensor , ADC_SampleTime_55_5Cycles);  
//   ADC_TempSensorCmd(ENABLE);
  
  /* Convert the ADC1 Vref  with 55.5 Cycles as sampling time */ 
  ADC_ChannelConfig(ADC1, ADC_Channel_1  , ADC_SampleTime_55_5Cycles); 
//   ADC_VrefintCmd(ENABLE);
  
  /* ADC Calibration */
  ADC_GetCalibrationFactor(ADC1);//不用等待校准结束,没那个函数
   //ADC_DMACmd(ADC1, ENABLE);//上面已经有了 ■■■ 
  /* Enable ADC1 */
  ADC_Cmd(ADC1, ENABLE);   
  
  /* Wait the ADCEN falg */
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADEN)); 
  
  /* ADC1 regular Software Start Conv */ 
  ADC_StartOfConversion(ADC1);
}

/////////////////////////////////////////////////////////////////////////

void DMA1_Channel1_IRQHandler(void)
{
	static char flag=0;
	
		if(DMA_GetITStatus(DMA1_IT_TC1))
		{
      //DMA_ClearFlag(DMA1_FLAG_TC1); 
			DMA_ClearITPendingBit(DMA1_IT_TC1);
			//done=1;
		}
	
		
		if(flag==0)
		{
		 flag=1;
		 GPIO_SetBits(GPIOA, GPIO_Pin_4);
		}
		else
		{
			flag=0;
	    GPIO_ResetBits(GPIOA, GPIO_Pin_4);
		}
}


void TIM1_IRQHandler()  
{  
    if(TIM_GetITStatus(TIM1, TIM_IT_Update))            //判断发生update事件中断  
    {  
        TIM_ClearITPendingBit(TIM1, TIM_IT_Update);     //清除update事件中断标志  
    }  
} 
