#ifndef __HX711_H
#define __HX711_H

#define HX711_DOUT(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_11):GPIO_ResetBits(GPIOA,GPIO_Pin_11)) 
#define HX711_SCK(n)  (n?GPIO_SetBits(GPIOA,GPIO_Pin_12):GPIO_ResetBits(GPIOA,GPIO_Pin_12)) 
#define HX711_DINPUT     GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_11)
#define GapValue  500

void HX711_Init(void);
unsigned long HX711_Read(void);
void Get_Weight(void);
void Get_Maopi(void);



#endif
