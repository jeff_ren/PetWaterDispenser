#ifndef __MS1112_H
#define __MS1112_H

#include "stm32f0xx.h"

#define  ms_deviceid_w          0x90
#define  ms_deviceid_r          0x91

uint8_t  ms1112_read_statereg(void);
uint16_t ms1112_read_data(uint16_t data);

#endif
