#include "tds.h"
#include "adc.h"
#include "flash.h"
#include "delay.h"

float TDS_value1 ;
float TDS_value2 ;
uint16_t Tds_Arguments;
uint16_t TDS_cal_value ; 
uint16_t Differ_Value ;

/***********************************
  * @brief  TDS GPIO口初始化
  * @param  none
  * @retval none
************************************/
void TDS_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* 使能GPIOA时钟 */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* 配置TDS相应引脚PA4(IO2),PA5(IO1)*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	IO1_0 ; // 初始化io脚
	IO2_1 ; // 初始化io脚
}

extern uint8_t TDS_Swich_Flag ; 
/***********************************
  * @brief  TDS 单次模式测量
  * @param  none
  * @retval TDS 等效电阻值
************************************/ 
float TDS_Single_Measure(void)
{
	uint16_t TDS_Origin ;
	float TDS_Value ; 
	
	while( TDS_Swich_Flag != 0 ) ; //等待第一次采集
	TDS_Origin = TDS_ADC_Vaulue();   // 获得TDS的ADC原始数据
	TDS_value1  =  ( TDS_Origin / 0xFFF ) * 3.3 ; // 第一次采集 即在IO1=0，IO2=1时候
	
	while( TDS_Swich_Flag != 1 ); //等待第二次采集
	TDS_Origin = TDS_ADC_Vaulue();   // 获得TDS的ADC原始数据
	TDS_value2  =  ( TDS_Origin / 0xFFF ) * 3.3 ; // 第二次采集 即在IO1=1，IO2=0时候
		
	TDS_Value = TDS_value1 / TDS_value2 ; // 采集两次之后计算一次 计算电阻值 单位是kΩ
	return TDS_Value ;
}


/***********************************
  * @brief  TDS 多次模式测量
  * @param  测量次数
  * @retval TDS 等效电阻值
************************************/
float TDS_Multi_Measure( uint8_t number )
{
	float TDS_Value ; 
	uint8_t TDS_Single_Value ;
	float TDS_Value_Sum = 0 ;
	uint8_t i ;

	for( i = 0 ; i < number ; i++ )
	{
		TDS_Single_Value = TDS_Single_Measure();
		TDS_Value_Sum = TDS_Value_Sum + TDS_Single_Value ; // 采集两次之后计算一次 计算电阻值 单位是kΩ 并进行累加
	}
	
	TDS_Value = TDS_Value_Sum / number ; //取平均值
	return TDS_Value ;
}

extern uint8_t TDS_Symbol;   //TDS补偿正负系数
extern uint16_t TDS_Fill;    // TDS补偿系数
/***********************************
  * @brief  TDS 接收处理
  * @param  none
  * @retval none
************************************/
uint16_t TDS_RxDispose(void)
{
	float TDS_value , TDS;
	uint16_t TDS_return ;
	TDS_value = TDS_Multi_Measure(10) ;
	TDS = 10000 / ( TDS_value * 2 ) ;  // TDS * (2 * 10-4) = 1 / Rx
	
	if(TDS_Symbol == 0x26)
  {
    TDS_return = (uint16_t)TDS + TDS_Fill;
  }
  else if(TDS_Symbol == 0x62)
  {
    TDS_return = (uint16_t)TDS - TDS_Fill;
  }
  else 
  {
    TDS_return = (uint16_t)TDS;
  }
	
	return TDS_return ;
}

extern uint16_t TDS_Calib_Value ; //TDS的校准值
extern uint8_t Cablib_Flag ;
extern uint8_t Cablib_Success , my_Flag; 
/***********************************
  * @brief  TDS 校准函数
  * @param  none
  * @retval none
************************************/
void TDS_Calibration(void)
{
	if( Cablib_Flag == 1 && my_Flag == 0)
		{
			my_Flag = 1 ;
			TDS_Fill = 0 ;    //清除系数
			TDS_Symbol = 0; 
			TDS_cal_value = TDS_RxDispose();  //获得原始数据

			if(TDS_Calib_Value >= TDS_cal_value)
			{
				Differ_Value = TDS_Calib_Value - TDS_cal_value ;
		 
				TDS_Fill = Differ_Value;
				TDS_Symbol = 0x26;
				Write_Data(17,TDS_Symbol);
				delay_ms(10);
				Write_Data(18,TDS_Fill % 256);
				delay_ms(10);
				Write_Data(19,TDS_Fill / 256);

				Cablib_Success = 0x01;
				
			}
			else if( TDS_Calib_Value < TDS_cal_value )
			{
				Differ_Value = TDS_cal_value - TDS_Calib_Value;
		 
				TDS_Fill = Differ_Value;
				TDS_Symbol = 0x62;
				Write_Data(17,TDS_Symbol);
				delay_ms(10);
				Write_Data(18,TDS_Fill % 256);
				delay_ms(10);
				Write_Data(19,TDS_Fill / 256);
				
				Cablib_Success = 0x01;
				
			}
	}
}
