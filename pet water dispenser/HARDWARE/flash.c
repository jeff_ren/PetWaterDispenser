#include "flash.h"
#include "delay.h"
#include "include.h"

/**
  ******************************************************************************
  *
  *                       内部flash存储
  *
  *              地址0 ：       存储标志位（是否第一次启动）
                 地址1-14 ：    传感器出厂信息 即 生产日期，使用日期
                 地址15 ：      PH校准系数
                 地址16-17 ：   PH校准数值
								 地址18 ：      温度校准系数
								 地址19 ：      温度校准数值
													
  ******************************************************************************
  */
	
	
uint8_t Flash_Temp[20];
uint8_t Flash_Buffer[30];
extern uint8_t Device_Message[14];
extern uint8_t T_Symbol,T_Value;
extern uint8_t Ver_Message[2];
extern uint16_t PH_Factor;            //PH校准系数
extern uint8_t  Symbol;               //符号系数
uint8_t Byte_Num = 30 ;

uint32_t EraseCounter = 0x00,Address = 0x00;
uint32_t NbrOfPage = 0x00;
__IO FLASH_Status FLASHStatus = FLASH_COMPLETE;

extern DATA_BUFFER data_buffer;
/***********************************
  * @brief  falsh 初始化
  * @param  none
  * @retval none
************************************/ 
void Flash_Init(void)
{

  data_buffer.pump_work_mode = NORMAL_MODE;

}

/***********************************
  * @brief  falsh 内数据读取(每次一个字)
  * @param  地址
  * @retval 数据
************************************/ 
uint8_t Read_Data(uint8_t addr)
{
	uint8_t data ;
	data =  *(uint32_t*)(FLASH_USER_START_ADDR+addr);
	return data ;
}

/***********************************
  * @brief  falsh 内数据写入(每次一个字)
  * @param  地址、数据
  * @retval none
************************************/ 
void Write_Data(uint8_t addr,uint8_t* data)
{
	uint16_t HalfWord ; 
  /* Unlock the Flash to enable the flash control register access *************/ 
  FLASH_Unlock();
    
  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  /* Clear pending flags (if any) */  
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR); 

  /* Define the number of page to be erased */
  NbrOfPage = (FLASH_USER_END_ADDR - FLASH_USER_START_ADDR) / FLASH_PAGE_SIZE;

  /* Erase the FLASH pages */
  for(EraseCounter = 0; (EraseCounter < NbrOfPage) && (FLASHStatus == FLASH_COMPLETE); EraseCounter++)
  {
    if (FLASH_ErasePage(FLASH_USER_START_ADDR + (FLASH_PAGE_SIZE * EraseCounter))!= FLASH_COMPLETE)
    {
     /* Error occurred while sector erase. 
         User can add here some code to deal with this error  */
      while (1)
      {
      }
    }
  }
  /* Program the user Flash area word by word
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  Address = FLASH_USER_START_ADDR;

  while (Byte_Num --)
  {
		HalfWord=*(data++);
    HalfWord|=*(data++)<<8;
    if (FLASH_ProgramHalfWord(Address, HalfWord) == FLASH_COMPLETE)
    {
      Address = Address + 2;
    }
    else
    { 
      /* Error occurred while writing data in Flash memory. 
         User can add here some code to deal with this error */
      while (1)
      {
      }
    }
  }
	
	  FLASH_Lock(); 
	
//	FLASH_Unlock();
//	FLASH_ClearFlag(FLASH_FLAG_BSY|FLASH_FLAG_EOP|FLASH_FLAG_PGERR|FLASH_FLAG_WRPERR);
//	FLASH_ErasePage(FLASH_USER_START_ADDR);
//	FLASH_ProgramWord(FLASH_USER_START_ADDR+addr,data);	
//	FLASH_ClearFlag(FLASH_FLAG_BSY|FLASH_FLAG_EOP|FLASH_FLAG_PGERR|FLASH_FLAG_WRPERR);
//	FLASH_Lock();
}

/***********************************
  * @brief  传感器信息写入
  * @param  数据
  * @retval none
************************************/ 
void Device_Write(uint8_t Data[])
{
  uint8_t j;
  for(j=0;j<14;j++)
  {
		Flash_Buffer[j+1] = Data[j] ;
  }
	Write_Data(0,Flash_Buffer);
  delay_ms(1);
}

/***********************************
  * @brief  传感器信息读取
  * @param  none
  * @retval none
************************************/ 
void Device_Read(void)
{

}

void Buffer_Read(void)
{
  uint8_t j;
  for(j=0;j<24;j++)
  {
    Flash_Buffer[j] = Read_Data(j);
    delay_ms(1);
  }
}
