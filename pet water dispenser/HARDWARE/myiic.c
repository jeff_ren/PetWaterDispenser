#include "myiic.h"
#include "delay.h"



void myiic_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* 使能GPIOA时钟 */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* 配置IIC相应引脚PA9(IIC_SCL),PA10(IIC_SDA)*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	IIC_SCL_1; // 初始化IIC IIC_SCL脚
	IIC_SDA_1;// 初始化IIC IIC_SDA脚
	IIC_SCL_0;
  I2C_Stop();
}


void I2C_Start(void)
{
	IIC_SDA_1;  
	delay_us(1);  
	IIC_SCL_1;  
	delay_us(1);  
	IIC_SDA_0;  
	delay_us(1);  
	IIC_SCL_0;  
	delay_us(1);
	
}


void I2C_Stop(void)
{   

		IIC_SCL_0;
		delay_us(1);
		IIC_SDA_0;
		delay_us(1);
		IIC_SCL_1;
		delay_us(1);
		IIC_SDA_1;
		delay_us(1);
}

void Master_Ack(void)
{
		IIC_SDA_0;
		delay_us(1);
		IIC_SCL_1;
		delay_us(1);

}

void Write(unsigned char data)
{
		unsigned char i=0;
		for(i=0;i<8;i++)
		{
				(data&0x80)?IIC_SDA_1:IIC_SDA_0;
				IIC_SCL_1;
				delay_us(1);
				IIC_SCL_0;
				delay_us(1);
		}
		IIC_SCL_1;
		delay_us(1);
		while(SDA);
//		IIC_SCL_0;
//		delay_us(1);
}


void WriteReg(unsigned char data)
{
		
		I2C_Start();
		I2C_Write_Byte(0x90);
		IIC_SCL_0;
		delay_us(1);
	  I2C_Write_Byte(data);
	  I2C_Stop();
	
}

unsigned char Read(void)
{
		unsigned char i=0;
		unsigned char Temp=0;
	  IIC_SCL_0;
		delay_us(1);
		IIC_SDA_1;  // ?????
		for(i=0;i<8;i++)
		{
		IIC_SCL_1;
		delay_us(1);
		Temp = Temp<<1;
				
				if(SDA)                   
				{
				Temp |= 0x01;
				}
				else
				{
				Temp &= ~0x01;
				}
		delay_us(1);
		IIC_SCL_0;
		delay_us(1);
		}
		Master_Ack();  // ???????????????
		return (Temp);
}

void RxFrame(unsigned char *p_Rx,unsigned char num)
{
		unsigned char i=0;
		//_disable_interrupts();//  ???,?????????? I2C
		I2C_Start();
		I2C_Write_Byte(SLAVE_ADDR);
		for(i=0;i<num;i++)
		{
				*p_Rx=Read();
				p_Rx++;
		}
		I2C_Stop();
		//_enable_interrupts();
}



void I2C_Write_Byte(uint8_t data)
{
		uint8_t i;
		for(i=0;i<8;i++)
		{

			(data&0x80)?IIC_SDA_1:IIC_SDA_0;
			delay_us(2);
			 IIC_SCL_1;
			delay_us(2);
			 IIC_SCL_0;
				delay_us(2);
			 data = data<<1; 
		} 

	
			IIC_SCL_0;
		delay_us(1);
//		IIC_SDA_1;// ?????,?????????
			IIC_SCL_1;
		delay_us(1);
		while(SDA);// ????
		
}


uint8_t I2C_Read_Byte(uint8_t flag)
{
		uint8_t i;
		uint8_t data = 0x00;
		IIC_SCL_0;
		IIC_SDA_1;
		for(i=0;i<8;i++)    //连续读取8位
		{
			IIC_SCL_1; 
			delay_us(2);
			data = data<<1;
				
				if(SDA)                   
				{
				data |= 0x01;
				}
				else
				{
				data &= ~0x01;
				}
				delay_us(1);
		}
		
		IIC_SCL_0;
		(flag)?IIC_SDA_0:IIC_SDA_1;   //产生ack或者nack应答信号
		delay_us(2);
		IIC_SCL_1;
		delay_us(2);
		IIC_SCL_0;
//		IIC_SDA_0;
//		IIC_SCL_1;
		return data;
}



void I2C_Write_Data(uint8_t addr,uint8_t *buff,uint16_t length)
{
	uint16_t i;
	addr = addr<<1;
	addr &= WRITE_MASK;
	I2C_Start();
	I2C_Write_Byte(addr);
	for(i=0;i<length;i++)
			{
				I2C_Write_Byte(buff[i]);
			}
	I2C_Stop();
}



void I2C_Read_Data(uint8_t addr,uint8_t *buff,uint16_t length)
{
		uint16_t i;
		addr = addr<<1;
		addr |= READ_MASK;
		I2C_Start();
		I2C_Write_Byte(addr);
		for(i=0;i<length;i++)
		{
			if(i==(length-1))
				{
					buff[i] = I2C_Read_Byte(0);
				}
			else
				{
					buff[i] = I2C_Read_Byte(1);
				}
		}
		I2C_Stop();
}

