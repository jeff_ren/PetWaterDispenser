#ifndef __FLASH_H
#define __FLASH_H

#include "stm32f0xx.h"

 #define FLASH_PAGE_SIZE         ((uint32_t)0x00000400)   /* FLASH Page Size */
 #define FLASH_USER_START_ADDR   ((uint32_t)0x08003400)   /* Start @ of user Flash area */
 #define FLASH_USER_END_ADDR     ((uint32_t)0x08004000)   /* End @ of user Flash area */
 
#define   Byte_Len   24

void Flash_Init(void);
uint8_t Read_Data(uint8_t addr);
void Write_Data(uint8_t addr,uint8_t* data);
void Device_Write(uint8_t Data[]);
void Device_Read(void);
void Buffer_Read(void);
#endif
