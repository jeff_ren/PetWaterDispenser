#ifndef __TIMER_H
#define __TIMER_H

#include "stm32f0xx.h"

void TIM2_Int_Init(uint16_t arr,uint16_t psc);
void TIM1_CC_IRQHandler(void) ;
#endif
