#include "rtc.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Uncomment the corresponding line to select the RTC Clock source */
#define RTC_CLOCK_SOURCE_LSE   /* LSE used as RTC source clock */
//#define RTC_CLOCK_SOURCE_LSI  // LSI used as RTC source clock. The RTC Clock
                                // may varies due to LSI frequency dispersion
__IO uint32_t AsynchPrediv = 0, SynchPrediv = 0;
RTC_TimeTypeDef RTC_TimeStruct;
RTC_DateTypeDef RTC_DateStruct;
RTC_InitTypeDef RTC_InitStruct;
RTC_AlarmTypeDef  RTC_AlarmStruct;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
#define BKP_VALUE    0x32F0 
void RTC_Exit(void)
{
	
  NVIC_InitTypeDef  NVIC_InitStruct;
  EXTI_InitTypeDef  EXTI_InitStruct;
	
	  /* RTC Alarm A Interrupt Configuration */
  /* EXTI configuration *********************************************************/
  EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStruct.EXTI_Line = EXTI_Line17;
  EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStruct.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStruct);
  
  /* Enable the RTC Alarm Interrupt */
  NVIC_InitStruct.NVIC_IRQChannel = RTC_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);
		
	}
	
	
///------------------------------------------------------------
//   * @brief  Configure the RTC peripheral by selecting the clock source.
//   * @param  None
//   * @retval None
//---------------------------------------------------------------------/
void RTC_Config(void)
{
  /* 使能 PWR 时钟 */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

  /* 允许访问RTC */
  PWR_BackupAccessCmd(ENABLE);
	
#if defined (RTC_CLOCK_SOURCE_LSI)  /* 当使用LSI 作为 RTC 时钟源*/
/* The RTC Clock may varies due to LSI frequency dispersion. */   
  /* 使能 LSI 振荡 */ 
//  RCC_HSEConfig(RCC_HSE_ON);
    RCC_LSICmd(ENABLE);
  /* 等待到 LSI 预备*/  
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

  /* 把RTC 时钟源配置为LSI */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
	

   /* 定襎同步分频值和异步分频值 */
  SynchPrediv = 0x334;
  AsynchPrediv = 0x2F;
	RTC_InitStruct.RTC_AsynchPrediv = AsynchPrediv;
  RTC_InitStruct.RTC_SynchPrediv = SynchPrediv;
  RTC_InitStruct.RTC_HourFormat = RTC_HourFormat_24;
  RTC_Init(&RTC_InitStruct);
	
#elif defined (RTC_CLOCK_SOURCE_LSE) /* 当使用LSE 最为 RTC 时钟源 */
  /*使能 LSE 振荡 */
  RCC_LSEConfig(RCC_LSE_ON);

  /*等待 LSE 预备 */  
  while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
  {
  }

  /* 把RTC 时钟源配置为使用LSE */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
     /* 定襎同步分频值和异步分频值 */
  SynchPrediv = 0xFF;
  AsynchPrediv = 0x7F;
	RTC_InitStruct.RTC_AsynchPrediv = AsynchPrediv;
  RTC_InitStruct.RTC_SynchPrediv = SynchPrediv;
  RTC_InitStruct.RTC_HourFormat = RTC_HourFormat_24;
  RTC_Init(&RTC_InitStruct);

#else
  #error Please select the RTC Clock source inside the main.c file
#endif /* RTC_CLOCK_SOURCE_LSI */
  
  /* 使能RTC时钟 */
  RCC_RTCCLKCmd(ENABLE);

  /* 等待 RTC APB 寄存器同步 */
  RTC_WaitForSynchro();
}


// -------------------------------------------------------------------------
//   * @brief  Returns the time entered by user, using Hyperterminal.
//   * @param  None
//   * @retval None
// ------------------------------------------------------------------------
void RTC_TimeRegulate(void)
{
  uint32_t tmp_hh = 0xFF, tmp_mm = 0xFF, tmp_ss = 0xFF;

  printf("\n\r==============Time Settings=====================================\n\r");
  RTC_TimeStruct.RTC_H12 = RTC_H12_AM;
  printf("  Please Set Hours:\n\r");
  while (tmp_hh == 0xFF)
  {
    tmp_hh = USART_Scanf(23);
    RTC_TimeStruct.RTC_Hours = tmp_hh;
  }
  printf("  %0.2d\n\r", tmp_hh);
  
  printf("  Please Set Minutes:\n\r");
  while (tmp_mm == 0xFF)
  {
    tmp_mm = USART_Scanf(59);
    RTC_TimeStruct.RTC_Minutes = tmp_mm;
  }
  printf("  %0.2d\n\r", tmp_mm);
  
  printf("  Please Set Seconds:\n\r");
  while (tmp_ss == 0xFF)
  {
    tmp_ss = USART_Scanf(59);
    RTC_TimeStruct.RTC_Seconds = tmp_ss;
  }
  printf("  %0.2d\n\r", tmp_ss);

  /* Configure the RTC time register */
  if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStruct) == ERROR)
  {
    printf("\n\r>> !! RTC Set Time failed. !! <<\n\r");
  } 
  else
  {
    printf("\n\r>> !! RTC Set Time success. !! <<\n\r");
    RTC_TimeShow();
    /* Indicator for the RTC configuration */
    RTC_WriteBackupRegister(RTC_BKP_DR0, BKP_VALUE);
  }

  tmp_hh = 0xFF;
  tmp_mm = 0xFF;
  tmp_ss = 0xFF;

  /* Disable the Alarm A */
  RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

  printf("\n\r==============Alarm A Settings=====================================\n\r");
  RTC_AlarmStruct.RTC_AlarmTime.RTC_H12 = RTC_H12_AM;
  printf("  Please Set Alarm Hours:\n\r");
  while (tmp_hh == 0xFF)
  {
    tmp_hh = USART_Scanf(23);
    RTC_AlarmStruct.RTC_AlarmTime.RTC_Hours = tmp_hh;
  }
  printf("  %0.2d\n\r", tmp_hh);
  
  printf("  Please Set Alarm Minutes:\n\r");
  while (tmp_mm == 0xFF)
  {
    tmp_mm = USART_Scanf(59);
    RTC_AlarmStruct.RTC_AlarmTime.RTC_Minutes = tmp_mm;
  }
  printf("  %0.2d\n\r", tmp_mm);
  
  printf("  Please Set Alarm Seconds:\n\r");
  while (tmp_ss == 0xFF)
  {
    tmp_ss = USART_Scanf(59);
    RTC_AlarmStruct.RTC_AlarmTime.RTC_Seconds = tmp_ss;
  }
  printf("  %0.2d", tmp_ss);

  /* Set the Alarm A */
  RTC_AlarmStruct.RTC_AlarmDateWeekDay = 0x31;
  RTC_AlarmStruct.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
  RTC_AlarmStruct.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;

  /* Configure the RTC Alarm A register */
  RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStruct);
  printf("\n\r>> !! RTC Set Alarm success. !! <<\n\r");
  RTC_AlarmShow();

  /* Enable the RTC Alarm A Interrupt */
  RTC_ITConfig(RTC_IT_ALRA, ENABLE);
   
  /* Enable the alarm  A */
  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
}

void RTC_ValueSet(void)
{
	RTC_TimeTypeDef temp_time;
	RTC_DateTypeDef temp_date;
	
	temp_time.RTC_Hours=0x11;
	temp_time.RTC_Minutes=0x11;
	temp_time.RTC_Seconds=0x00;
	
	temp_date.RTC_Date = 0x11;
	temp_date.RTC_Month = 0x10;
	temp_date.RTC_WeekDay=0x04;
	temp_date.RTC_Year = 0x18; 
	RTC_SetTime(RTC_Format_BCD,&temp_time);
  RTC_SetDate(RTC_Format_BCD,&temp_date);

}
// ------------------------------------------------------------------
//   * @brief  显示当前时间.
//   * @param  None
//   * @retval None
// ------------------------------------------------------------------
void RTC_TimeShow(void)
{
  /* 获得当前时间 */
  RTC_GetTime(RTC_Format_BCD, &RTC_TimeStruct);
	RTC_GetDate(RTC_Format_BCD, &RTC_DateStruct);
	/* lcd上显示 */
 // printf("\n\r  The current time is :  %0.2d:%0.2d:%0.2d \n\r", RTC_TimeStruct.RTC_Hours, RTC_TimeStruct.RTC_Minutes, RTC_TimeStruct.RTC_Seconds);
}

// ---------------------------------------------------------
//   * @brief 显示报警时间.
//   * @param  None
//   * @retval None
// ----------------------------------------------------------
void RTC_AlarmShow(void)
{
  /* 获取当前报警时间*/
  RTC_GetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStruct);
	/* lcd上显示 */
	
//  printf("\n\r  The current alarm is :  %0.2d:%0.2d:%0.2d \n\r", RTC_AlarmStruct.RTC_AlarmTime.RTC_Hours, RTC_AlarmStruct.RTC_AlarmTime.RTC_Minutes, RTC_AlarmStruct.RTC_AlarmTime.RTC_Seconds);
}


// ------------------------------------------------------
//   * @brief  Gets numeric values from the hyperterminal.
//   * @param  None
//   * @retval None
// ------------------------------------------------------
uint8_t USART_Scanf(uint32_t value)
{
  uint32_t index = 0;
  uint32_t tmp[2] = {0, 0};

  while (index < 2)
  {
    /* Loop until RXNE = 1 */
    while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET)
    {}
    tmp[index++] = (USART_ReceiveData(USART1));
    if ((tmp[index - 1] < 0x30) || (tmp[index - 1] > 0x39))
    {
      printf("\n\r Please enter valid number between 0 and 9 \n\r");
      index--;
    }
  }
  /* Calculate the Corresponding value */
  index = (tmp[1] - 0x30) + ((tmp[0] - 0x30) * 10);
  /* Checks */
  if (index > value)
  {
    printf("\n\r Please enter valid number between 0 and %d \n\r", value);
    return 0xFF;
  }
  return index;
}
	
	
	
	
	