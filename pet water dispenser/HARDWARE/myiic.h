#ifndef __MYIIC_H
#define __MYIIC_H

#include "stm32f0xx.h"

#define WRITE_MASK 0xFE
#define READ_MASK  0x01
#define SLAVE_ADDR 0x91

//SDA,SCL输出端口定义
#define IIC_SCL_1                         GPIO_SetBits(GPIOA, GPIO_Pin_9)
#define IIC_SCL_0                         GPIO_ResetBits(GPIOA, GPIO_Pin_9)
#define IIC_SDA_1                         GPIO_SetBits(GPIOA, GPIO_Pin_10)
#define IIC_SDA_0                         GPIO_ResetBits(GPIOA, GPIO_Pin_10)
//SDA输入端口定义
#define SDA     GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_10)

void myiic_init(void);
void I2C_Start(void);
void I2C_Stop(void);
void I2C_Write_Byte(uint8_t data);
uint8_t I2C_Read_Byte(uint8_t flag);
void I2C_Write_Data(uint8_t addr,uint8_t *buff,uint16_t length);
void I2C_Read_Data(uint8_t addr,uint8_t *buff,uint16_t length);
void I2C_Start(void);
void I2C_Stop(void);
void Master_Ack(void);
void Write(unsigned char data);
unsigned char Read(void);
void WriteReg(unsigned char data);
void RxFrame(unsigned char *p_Rx,unsigned char num);


#endif
