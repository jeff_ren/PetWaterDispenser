/**
  ******************************************************************************
  * @file    main.c
  * @author  JeffRen
  * @version V0.0
  * @date    2018-08-08
  * @brief   宠物饮水机
  ******************************************************************************
  * @attention
  *                       杭州吉印智能科技有限公司
  *
  ******************************************************************************
  */
#include "include.h"

// ADC1通过DMA转换的电压值
extern __IO uint16_t  RegularConvData_Tab;
// 局部变量，用于存从flash读到的电压值			 
__IO uint16_t ADC_ConvertedValueLocal;
extern uint8_t pump_lack_flag ;
uint16_t fakeTDS ;
uint8_t PutInWater_Flag ;
uint8_t fakeTDS_count_flag ;
void display(void);
void FakeTDS(void)
{
	if( pump_lack_flag == 1 )
	{		// 缺水情况下变成0
		fakeTDS = 0 ;
		PutInWater_Flag = 1 ; //从水中拿出
		TDS_B_LED(OFF);
	}		
	else if ( pump_lack_flag == 0 ) // 有水的状况下进行检测
	{
		if( PutInWater_Flag == 1 ) // 刚放入水中
		{
			fakeTDS_count_flag = 1 ; //延时操作开启，串口打印“正在获取数据”
		}
		else if( PutInWater_Flag == 0 ) // 已放入水中一段时间
		{
			fakeTDS = 148 ;
			TDS_B_LED(ON);
		}
	}
}

/***********************************
  * @brief  初始化
  * @param  none
  * @retval none
************************************/
void SysInit(void)
{
	SystemInit(); //系统初始化
	delay_init(); //延时初始化
	KEY_Init();
	Flash_Init(); //Flash初始化 读取相应数据
//	ADC_Config(); //ADC初始化
	USART_Configuration();
//	RTC_Config();
//	RTC_ValueSet();
	TIM2_Int_Init(9999,479); //48M/480=100KHZ 10000*10us=100ms
  HX711_Init();
	Pump_Init();//水泵初始化
	LED_Init();
}

/***********************************
  * @brief  主程序入口
  * @param  none
  * @retval none
************************************/
int main(void)
{
	SysInit();
	delay_ms(3000);
	Get_Maopi();
	printf("\n\r  *********************** RTC Hardware Calendar Example ***********************\n\r");
  while (1)
  {	
    Key_Scan();
		Get_Weight();
		RTC_TimeShow();
		Pump_Dispose();
		FakeTDS();
//    Data_RxDispose();  //数据处理函数
//	  trans_flag = 0 ;   //捕获数据标志位清0
//	  Calib_Dispose();   //校准处理函数
		delay_ms(50);
		display();
	
  }
}

extern unsigned char Flag_ERROR;
extern long Weight_Shiwu;
void display(void)
{
  printf("\n\r");
	
	if( fakeTDS_count_flag == 1 ) // 刚放入水中
		printf("\n\r正在获取TDS数据\n\r");
	else
		printf("\n\r TDS值 = %d \n\r",fakeTDS);
//  if(Flag_ERROR == 1)
//		printf("\n\r ERROR，当前重量为负 \n\r");
//	else
    printf("\n\r 当前重量 = %ld g\n\r",Weight_Shiwu);
	printf("\n\r");
}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t* file, uint32_t line)
{

  while (1)
  {
  }
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
