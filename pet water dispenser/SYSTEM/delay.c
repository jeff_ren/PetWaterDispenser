#include "delay.h"

void delay_init(void)
{
	SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;	
	SysTick->LOAD = 47;					//48-1
}
 
void delay_ms(int16_t nTime)
{
	nTime *= 1000;
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;		
	while(nTime--){
		while((SysTick->CTRL&0X010000) == 0);		
	}
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE_Msk);		
}
 
void delay_us(int16_t nTime)
{
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
	while(nTime--){
		while((SysTick->CTRL&0X010000) == 0);
	}
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE_Msk);
}
