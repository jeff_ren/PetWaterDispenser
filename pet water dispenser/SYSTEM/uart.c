
#include "uart.h"
#include "include.h"
#include "stdio.h"
void USART_Configuration(void)
  {     
     GPIO_InitTypeDef  GPIO_InitStructure;
        USART_InitTypeDef USART_InitStructure;
                
        RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE );
                
        GPIO_PinAFConfig(GPIOA,GPIO_PinSource2,GPIO_AF_1);
        GPIO_PinAFConfig(GPIOA,GPIO_PinSource3,GPIO_AF_1);        
        /*
        *  USART1_TX -> PA2 , USART1_RX ->        PA3
        */                                
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_3;                 
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; 
        GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
        GPIO_Init(GPIOA, &GPIO_InitStructure);        
        
        USART_InitStructure.USART_BaudRate = 115200;//设置串口波特率
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;//设置数据位
        USART_InitStructure.USART_StopBits = USART_StopBits_1;//设置停止位
        USART_InitStructure.USART_Parity = USART_Parity_No;//设置效验位
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//设置流控制
        USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;//设置工作模式
        USART_Init(USART2, &USART_InitStructure); //配置入结构体

        USART_Cmd(USART2, ENABLE);//使能串口1

		}			
	                                 

 	int fputc(int ch, FILE *f)
 {
   /* Place your implementation of fputc here */
   /* e.g. write a character to the USART */
   USART_SendData(USART2, (uint8_t) ch);

   /* Loop until the end of transmission */
   while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET)
   {
   }

   return ch;
 }
